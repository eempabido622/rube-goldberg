<?php include "./header.html"; ?>

<main id="main">
    <div class="page-banner bleed">
      <div class="container">
        <div class="image-wrap"><img src="./img/placeholder/Experience-Rube-Banner.jpg" alt="Experience Rube" /></div>
      </div>
    </div><!-- End of page-banner -->

  <div class="page-content py-10 md:pt-16 md:pb-20">
    <div class="container bg-no-repeat lg:bg-[right_5rem_top_5rem] lg:bg-illustration-vice">
      <article class="content">
        <div class="text-block float on-full bg-off-white">
          <h1 class="page-title">Experience Rube</h1>
          <p>Today Rube Goldberg Machines are searchable, hashtag-able and viral. Millions of results on Google, and hundreds of millions of views YouTube, Tik-Tok and other media outlets prove the power of the brand.</p>
        </div>

        <div class="wrapper mb-7 md:mb-12 flex flex-wrap justify-between items-start gap-y-0 gap-x-4">
            <div class="tabs basis-full lg:basis-4/5">
                <a href="#" class="toggle block lg:hidden"><span class="text">All</span></a> 
                <ul class="uppercase xl">
                    <li><a href="#">Rube Around Town</a></li>
                    <li><a href="#" class="active">Who's Building Rube</a></li>
                </ul>
            </div>
        </div>

        <p class="mb-10">Explainer text here.</p>

        <div class="card v4 mb-10 md:mb-16">
            <div class="divider mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h3 mb-2.5">Brands</h2>
                  <p>The Institute works with TV studios, advertisers, and brands to bring Rube Goldberg Machines® to life. Let us help you bring our world of Rube into yours. Contact us.</p>
                  <iframe class="mb-12" width="560" height="368" src="https://www.youtube.com/embed/zPVH2admAuw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <p class="mb-8">These are just some of the brands inspired by Rube Goldberg. View more on Rube Goldberg TV!</p>
                  <div class="wrapper mb-12 grid grid-cols-3 gap-4 md:gap-x-4 md:gap-y-9 items-center">
                      <div class="item"><a href="#"><img src="./img/placeholder/Nike.svg" alt="Nike" class="mx-auto" /></a></div>
                      <div class="item"><a href="#"><img src="./img/placeholder/Google.svg" alt="Google" class="mx-auto" /></a></div>
                      <div class="item"><a href="#"><img src="./img/placeholder/Volvo.svg" alt="Volvo" class="mx-auto" /></a></div>
                      <div class="item"><a href="#"><img src="./img/placeholder/3M.svg" alt="3M" class="mx-auto" /></a></div>
                      <div class="item"><a href="#"><img src="./img/placeholder/honda.svg" alt="Honda" class="mx-auto" /></a></div>
                      <div class="item"><a href="#"><img src="./img/placeholder/Beneful.svg" alt="Beneful" class="mx-auto" /></a></div>
                  </div>
                  <div class="task-table grid grid-cols-1 md:grid-cols-2 gap-4">
                    <div class="column">
                        <table class="table-auto">
                            <tr><td>2D Photography</td></tr>
                            <tr><td>Barclays</td></tr>
                            <tr><td>Bauer Electronics</td></tr>
                            <tr><td>Bill & Melinda Gates Foundation</td></tr>
                            <tr><td>Canon</td></tr>
                            <tr><td>Coca-Cola</td></tr>
                            <tr><td>Crayola</td></tr>
                            <tr><td>Dominos</td></tr>
                            <tr><td>ESPN</td></tr>
                            <tr><td>Fedex</td></tr>
                            <tr><td>General Mills</td></tr>
                            <tr><td>Google Doodle</td></tr>
                            <tr><td>Guiness World Records</td></tr>
                            <tr><td>Harlem Globetrotters</td></tr>
                            <tr><td>Hud-Airbus</td></tr>
                            <tr><td>Infinity QX50</td></tr>
                            <tr><td>LG</td></tr>
                        </table>
                    </div>
                    <div class="column">
                        <table class="table-auto">
                            <tr><td>Microsoft</td></tr>
                            <tr><td>NCAA March Madness</td></tr>
                            <tr><td>Nerds</td></tr>
                            <tr><td>Nestle</td></tr>
                            <tr><td>Nokia</td></tr>
                            <tr><td>Panera Bread</td></tr>
                            <tr><td>Post-It</td></tr>
                            <tr><td>Qualcomm</td></tr>
                            <tr><td>Red Bull</td></tr>
                            <tr><td>Samsung</td></tr>
                            <tr><td>Seiko</td></tr>
                            <tr><td>Smartway</td></tr>
                            <tr><td>Southwest Airlines</td></tr>
                            <tr><td>Spinmaster</td></tr>
                            <tr><td>Target</td></tr>
                            <tr><td>TV2-Denmark</td></tr>
                        </table>
                    </div>
                    </div>
                </div>
                <div class="image hidden"></div>
            </div>
        </div>
        <div class="card v4 mb-0">
            <div class="divider mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                    <h2 class="h3 mb-2.5">Build Your Own Rube Goldberg Machine</h2>
                    <p>Do you Rube? Join our community of problem-solvers, young and old, who design and build Rube Goldberg Machines. International competitions, along with professional builders, humorists and influencers have made Rube not just an activity, but a verb.</p>
                    <p>Enjoy our playlist of lorem ipsum dolor sit amet!</p>
                    <iframe class="mb-5" width="560" height="368" src="https://www.youtube.com/embed/zPVH2admAuw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <p class="text-sm">Caption explaining JH Page Turner</p>
                </div>
                <div class="image hidden"></div>
            </div>
        </div>
      </article><!-- End of content -->
    </div>
  </div><!-- End of page-content -->

  <div class="container">
    <div class="call-to-action mb-0.5 md:mb-20">
      <div class="text bg-soft-black-pattern">
        <p class="text-white font-400 mb-8">From board games and toys to music videos and Hollywood movies, Rube has influenced some of the most indelible moments in pop culture.</p>
        <div class="btn-row"><a href="#" class="btn block-white">Learn About Rube</a></div>
      </div>
      <div class="image"><img src="./img/placeholder/Rube-Resources-cta.jpg" alt="Learn About Rube" /></div>
    </div><!-- End of call-to-action -->
  </div>
</main><!-- End of main -->

<?php include "./footer.html"; ?>