<?php include "./header.html"; ?>

<main id="main">
    <div class="page-banner bleed">
      <div class="container">
        <div class="image-wrap"><img src="./img/placeholder/Experience-Rube-Banner.jpg" alt="Experience Rube" /></div>
      </div>
    </div><!-- End of page-banner -->

  <div class="page-content py-10 md:pt-16 md:pb-20 ">
    <div class="container">
      <article class="content">
        <div class="text-block float on-full bg-off-white">
        <h1 class="page-title">Experience Rube</h1>
          <p>Today Rube Goldberg Machines are searchable, hashtag-able and viral. Millions of results on Google, and hundreds of millions of views YouTube, Tik-Tok and other media outlets prove the power of the brand.</p>
        </div>

        <div class="wrapper mb-5 md:mb-5 flex flex-wrap justify-between items-start gap-y-0 gap-x-4">
            <div class="tabs basis-full lg:basis-4/5">
                <a href="#" class="toggle block lg:hidden"><span class="text">All</span></a> 
                <ul class="uppercase xl">
                    <li><a href="#" class="active">Rube Around Town</a></li>
                    <li><a href="#">Who's Building Rube</a></li>
                </ul>
            </div>
        </div>

        <p class="mb-5">Explainer text here.</p>

        <div class="card v4 mb-10 md:mb-16">
            <div class="divider hidden md:block mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h4 mb-2.5"><em>Rube Goldberg: The World of Hilarious Invention Exhibit</em></h2>
                  <p>Inspired by Rube’s original illustrations and inventive storytelling, this traveling exhibit contains a collection of new 3D, life-size machines and hands-on, interactive components that connect Rube’s iconic cartoon contraptions to the way things work in the physical world. </p>
                  <p><strong>Upcoming dates:</strong><br>Fall 2021: <a href="#">Children’s Museum of Atlanta, Atlanta, GA</a> <br>Spring 2022: <a href="#">Please Touch Museum, Philadelphia, PA</a> <br>Summer 2022: <a href="#">Kalamazoo Valley Museum, Kalamazoo, MI</a></p>
                  <p>Interested in booking the exhibit? <a href="#">Contact us</a>.</p>
                </div>
                <div class="image"><img src="./img/placeholder/around-town-1.png" alt="Rube Goldberg: The World of Hilarious Invention Exhibit" /></div>
            </div>
        </div>
        <div class="card v4 mb-10 md:mb-16">
            <div class="divider hidden md:block mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h4 mb-2.5">Rube Goldberg TV on YouTube</h2>
                  <p class="mb-7">Watch Rube Goldberg TV, our YouTube channel devoted exclusively to great Rube Goldberg Machines.  </p>
                  <div class="btn-wrap">
                      <a href="#" class="btn outline-black">Tune In</a>
                  </div>
                </div>
                <div class="image"><img src="./img/placeholder/around-town-2.png" alt="Rube Goldberg TV on YouTube" /></div>
            </div>
        </div>
        <div class="card v4">
            <div class="divider hidden md:block mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h4 mb-2.5">Miracle Machine Playgrounds</h2>
                  <p class="mb-7">Miracle Machines are free-standing Rube Goldberg interactive play panels for public spaces. They are part of the Playground Project — an international design challenge to create a Rube Goldberg playground where kids become part of the machines. Sponsored by Playpower. Dates and details will be announced in 2023.</p>
                  <div class="btn-wrap">
                      <a href="#" class="btn outline-black">Learn More</a>
                  </div>
                </div>
                <div class="image"><img src="./img/placeholder/around-town-3.png" alt="Miracle Machine Playgrounds" /></div>
            </div>
        </div>

      </article><!-- End of content -->
    </div>
  </div><!-- End of page-content -->

  <div class="container">
    <div class="call-to-action mb-0.5 md:mb-20">
      <div class="text bg-soft-black-pattern">
        <p class="text-white font-400 mb-8">From board games and toys to music videos and Hollywood movies, Rube has influenced some of the most indelible moments in pop culture.</p>
        <div class="btn-row"><a href="#" class="btn block-white">Learn About Rube</a></div>
      </div>
      <div class="image"><img src="./img/placeholder/experience-rube-around-town-cta.png" alt="Learn About Rube" /></div>
    </div><!-- End of call-to-action -->
  </div>
</main><!-- End of main -->

<?php include "./footer.html"; ?>