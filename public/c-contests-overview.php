<?php include "./header.html"; ?>

<main id="main">
    <div class="page-banner bleed">
      <div class="container">
        <div class="image-wrap"><img src="./img/placeholder/contest-landing-collage-2x.jpg" /></div>
      </div>
    </div><!-- End of page-banner -->

  <div class="page-content py-10 md:pt-16 md:pb-20">
    <div class="container flex flex-wrap gap-y-10 lg:gap-[8%]">
      <article class="content basis-full lg:basis-[55%]">
        <div class="text-block float bg-off-white">
          <h1 class="page-title">Rube Goldberg Contests</h1>
          <p>Since 1988 the annual Rube Goldberg Machine Contest challenges students from all around the world to imagine, design and build working Rube Goldberg Machines that in the most overly complicated way complete a simple, singular task. With thousands of students participating internationally, whether live, online, or in the digital world of Minecraft, Rube Goldberg is its own universal language.</p>
        </div>
        
        <a href="#" class="card v1 mb-7 bg-white">
          <div class="image"><img src="./img/placeholder/rube-goldberg-machine-contest-middle-school.jpg" alt="The Rube Goldberg Machine Contest®" /></div>
          <div class="details">
            <span class="kicker mb-3.5">Build</span>
            <h2 class="h5 mb-3">The Rube Goldberg Machine Contest®</h2>
            <p class="mb-4">Build, film, and submit your working Rube Goldberg Machine and enter the contest. </p>
            <p class="mb-0">All ages — and free!</p>
          </div>
        </a>
        <a href="#" class="card v1 mb-7 bg-white">
          <div class="image"><img src="./img/placeholder/cartoon-contest-preview-2x.jpg" alt="The Rube Goldberg Crazy Contraption Cartoon Contest®" /></div>
          <div class="details">
            <span class="kicker mb-3.5">Draw</span>
            <h2 class="h5 mb-3">The Rube Goldberg Crazy Contraption Cartoon Contest®</h2>
            <p class="mb-4">Draw your own Rube Goldberg invention cartoon and send it in to enter the competition!</p>
            <p class="mb-0">All ages — and free!</p>
          </div>
        </a>
        <a href="#" class="card v1 mb-7 bg-white">
          <div class="image"><img src="./img/placeholder/minecraft-contest-preview-2x.jpg" alt="The Rube Goldberg/Minecraft Competition" /></div>
          <div class="details">
            <span class="kicker mb-3.5">Imagine</span>
            <h2 class="h5 mb-3">The Rube Goldberg/Minecraft Competition</h2>
            <p class="mb-4">Imagine and create your Rube Goldberg Machine in the digital world of Minecraft and join NASEF’s worldwide challenge!</p>
            <p class="mb-0">All ages — and free!</p>
          </div>
        </a>
        <a href="#" class="card v1 mb-10 md:mb-16 bg-white">
          <div class="image"><img src="./img/placeholder/lake-park-high-school-wins-argonnes-2017-rube-goldberg-machine-challenge.jpg" alt="The Rube Goldberg STEM Challenge®" /></div>
          <div class="details">
            <span class="kicker mb-3.5">Host Your Own</span>
            <h2 class="h5 mb-3">The Rube Goldberg STEM Challenge®</h2>
            <p class="mb-4">Host your own Official Rube Goldberg STEM Challenge This is a licensed product for live competition. </p>
            <p class="mb-0">All ages and for 3–10 teams.</p>
          </div>
        </a>

        <h5 class="mb-7">Contest winners have been featured on...</h5>
        <div class="flex flex-wrap justify-between items-center gap-4">
          <div class="item"><a href="#"><img src="./img/placeholder/NYT.svg" alt="NYT" /></a></div>
          <div class="item"><a href="#"><img src="./img/placeholder/Jimmy-Kimmel.svg" alt="Jimmy Kimmel" /></a></div>
          <div class="item"><a href="#"><img src="./img/placeholder/Today-Show.svg" alt="Today Show" /></a></div>
          <div class="item"><a href="#"><img src="./img/placeholder/CNN.svg" alt="CNN" /></a></div>
          <div class="item"><a href="#"><img src="./img/placeholder/CBS.svg" alt="CBS" /></a></div>
        </div>
      </article><!-- End of content -->

      <aside class="sidebar basis-full lg:basis-[37%]">
        <div class="widget mb-10 text-widget">
          <div class="divider mb-3.5 border-t-[5px] border-soft-black"></div>
          <h3 class="widget-title mb-6">This Year’s Task</h3>
          <p><img src="./img/placeholder/contest-badge-and-sponsors.svg" width="440" height="170"></p>
          <p>Each year our contests share a theme and for 2022 the focus is on literacy. This year's task: OPEN A BOOK, is sponsored by <a href="#">SYLVANIA General Lighting</a>, the makers of TruWave technology — the best reading light in the world!</p>
        </div>

        <div class="widget mb-10 text-widget">
          <div class="divider mb-3.5 border-t-[5px] border-soft-black"></div>
          <h3 class="widget-title mb-9">More ways to compete</h3>
          <div class="post v1 mb-10">
            <a href="#" class="image rounded-full"><img src="./img/placeholder/Badge_Machine Contest.png" width="120" height="120" alt="First Robotics Contest" /></a>
            <div class="details">
              <h3 class="h5 mb-1.5">First Robotics Contest</h3>
              <p class="mb-2.5">Learn how to combine simple machines to make your own Rube Goldberg Machine.</p>
              <p class="mb-0">
                <a href="#" class="link">
                  <strong>Register</strong>
                  <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M15.8401 10.4082H15.5492H15.3146H14.8454H14.3386C14.2762 10.402 14.2133 10.402 14.1509 10.4082H13.7287C13.6776 10.3982 13.6249 10.4015 13.5755 10.4179C13.5262 10.4342 13.4819 10.4629 13.4472 10.5013C13.3345 10.6501 13.4941 10.5013 13.4472 10.7897V11.0595L13.5128 11.441C13.5128 11.441 13.5128 11.869 13.5128 12.0737C13.5128 12.2784 13.5128 12.3715 13.5128 12.5017V12.9577L13.4565 13.8881H12.978C11.9904 13.9977 10.997 14.0475 10.0033 14.037C8.98979 14.037 7.97633 14.037 6.96286 14.037H2.1583V6.59336L2.23338 6.03509C2.01755 5.6443 2.13953 5.45821 2.11138 5.10464C2.10379 4.74115 2.11946 4.37754 2.1583 4.01602C2.1583 3.62523 2.11138 3.23444 2.1583 2.81574H5.23623C5.30617 2.78556 5.37832 2.76068 5.45205 2.7413C5.45205 2.7413 5.45205 2.49007 5.45205 2.29468C5.45205 2.09928 5.45205 1.61545 5.45205 1.2991C5.44144 1.11804 5.41951 0.937803 5.38637 0.759437C5.38637 0.619869 5.38638 0.666397 5.17055 0.619875H2.27091C2.05508 0.619875 1.79233 0.545439 1.64219 0.619875C1.53092 0.683573 1.4047 0.7171 1.27621 0.7171C1.14773 0.7171 1.02151 0.683573 0.910241 0.619875C0.667536 0.584878 0.421002 0.584878 0.178297 0.619875L0 0.926911V16.177H4.23215C4.55023 16.1323 4.87457 16.1741 5.17055 16.298C5.25699 16.3321 5.34968 16.348 5.44267 16.3445C5.5346 16.3385 5.62418 16.3131 5.70542 16.2701C6.11831 16.0747 6.5312 16.2235 6.93471 16.177C7.43319 16.2896 7.94769 16.3147 8.45492 16.2514C9.05627 16.151 9.66829 16.1291 10.2754 16.1863C10.951 16.1863 11.6361 16.1863 12.3305 16.1863L13.2219 16.1212L15.5679 15.9072C15.5679 15.8048 15.6336 15.7211 15.6711 15.6373L15.9339 15.5536V10.7804C15.9714 10.3524 15.8401 10.4082 15.8401 10.4082ZM6.93471 9.1521L7.235 9.46846L7.6479 9.82203L7.7605 9.89647C7.76931 9.90637 7.78014 9.9143 7.79227 9.91973C7.80441 9.92516 7.81756 9.92797 7.83088 9.92797C7.84419 9.92797 7.85735 9.92516 7.86948 9.91973C7.88162 9.9143 7.89245 9.90637 7.90125 9.89647L8.38922 9.59872L8.5206 9.40333L8.64259 9.31958L9.07424 8.9288V8.7241L9.25254 8.65897L9.43084 8.54732L10.0033 7.92391L10.1628 7.74713L10.2942 7.59825L10.4443 7.52381L12.49 5.56987L12.7058 5.35587C12.8036 5.19593 12.9134 5.04352 13.0343 4.89994L13.2782 4.83481L13.541 4.56498V4.69525L13.4941 5.72804C13.4892 5.89677 13.4704 6.06482 13.4378 6.23049V6.74224V6.92833C13.4378 8.02626 13.3533 8.09139 13.663 8.49148C13.8131 8.64036 13.8976 8.59384 14.0853 8.57523C14.6127 8.62573 15.1437 8.62573 15.6711 8.57523C15.7877 8.45315 15.8688 8.30203 15.9057 8.13792C15.9329 7.9008 15.9138 7.66072 15.8494 7.43077C15.8307 7.23582 15.8307 7.03954 15.8494 6.84459C15.8201 6.53512 15.8201 6.22361 15.8494 5.91414C15.843 5.45444 15.8618 4.99473 15.9057 4.53707C15.9057 4.32306 15.962 3.9881 15.9996 3.66245C16.0039 3.25458 15.9756 2.84701 15.9151 2.44355C15.9151 2.21094 15.9151 2.03416 15.9151 1.66198C15.9151 1.54102 16.0371 1.25257 15.9808 1.13161C15.899 0.977932 15.7911 0.839438 15.6618 0.722225C15.5491 0.610571 15.2301 0.824575 15.08 0.722225C14.837 0.690642 14.591 0.690642 14.348 0.722225H14.0196C13.6161 0.722225 13.3345 0.722225 12.931 0.722225C12.7414 0.693042 12.5503 0.674391 12.3586 0.666386C12.1067 0.703828 11.853 0.728673 11.5985 0.740824C11.2795 0.740824 11.2044 0.685004 10.6601 0.675699C10.1159 0.666395 10.8103 0.675699 10.1253 0.675699C9.83435 0.675699 9.53405 0.675699 9.23377 0.675699H8.62382H8.41737C7.97633 0.675699 7.78865 0.591961 7.47898 0.98275C7.32779 1.47712 7.29564 1.99977 7.38514 2.50869C7.5822 3.03905 7.56343 2.75991 8.00448 2.82504C8.2376 2.86192 8.47515 2.86192 8.70827 2.82504C8.89548 2.80638 9.08411 2.80638 9.27131 2.82504H9.48714H9.69359C9.86511 2.80644 10.0382 2.80644 10.2097 2.82504C10.4349 2.82504 10.6695 2.82504 10.8947 2.82504H11.1856H11.7017H12.1897H12.3492L6.51244 8.48218C6.4186 8.56593 6.44675 8.59383 6.51244 8.69618C6.57813 8.79853 6.87841 9.09628 6.93471 9.1521Z" fill="#4EA3AA"/>
                  </svg>
                </a>
              </p>
            </div>
          </div>
          <div class="post v1 mb-0">
            <a href="#" class="image rounded-full"><img src="./img/placeholder/Badge_Machine_Contest_2.png" width="120" height="120" alt="Rube Goldberg Hackathon" /></a>
            <div class="details">
              <h3 class="h5 mb-1.5">Rube Goldberg Hackathon</h3>
              <p class="mb-2.5">A host-authored contest created for college students and corporate teams to build together in a 24- or 48-hour sprint.</p>
              <p class="mb-0">
                <a href="#" class="link">
                  <strong>Details</strong>
                  <svg width="12" height="19" viewBox="0 0 12 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.79699 11.0847L5.93728 10.9572C6.47064 10.6145 6.9397 9.72754 6.9397 9.35254C6.96977 9.30523 6.99801 9.2732 7.02507 9.26101C7.18757 9.18778 6.95736 8.95818 6.82195 8.85254L6.32195 8.35254L5.59294 7.85815L5.13383 7.45005L4.33038 6.73587C4.12632 6.55732 3.94778 6.35327 3.75648 6.16197C3.51325 5.89504 3.25342 5.64373 2.97853 5.40953C2.52333 5.01563 2.09712 4.58942 1.70321 4.13421C1.55042 3.95591 1.38414 3.78963 1.20584 3.63684C0.964678 3.50068 0.748934 3.32377 0.568174 3.11395C0.249344 2.68035 -0.146007 2.45079 0.134564 1.83863C0.455328 1.25674 0.969712 0.80553 1.58843 0.56331C2.18783 0.499544 2.6342 1.03518 2.97853 1.43053L3.14433 1.62183L3.62895 2.15746C3.87126 2.42528 4.10082 2.6931 4.35588 2.93541C4.94253 3.49655 5.69497 3.95567 6.06481 4.42754C6.43466 4.89941 7.05956 5.67735 7.34013 5.94517C7.62071 6.21299 8.05431 6.57008 8.22011 6.69761C8.3859 6.82514 8.51343 7.10571 8.65372 7.23324C9.01081 7.53932 9.3679 7.66685 9.71223 7.99844L9.94179 8.228C10.0821 8.35553 10.5157 8.62334 10.5922 8.80189C10.6687 8.98043 11.1151 9.06971 11.1151 9.26101C11.1226 9.46784 11.0968 9.67455 11.0386 9.87316C10.9876 10.0262 10.4774 10.3068 10.3882 10.4215C10.1076 10.7659 9.89078 10.8552 9.72499 11.072C9.43083 11.3617 9.10581 11.6183 8.75574 11.8372C8.44967 12.0795 8.13083 12.3218 7.93954 12.5003C7.48644 12.9192 7.06047 13.3664 6.66421 13.8394L5.91177 14.6556L5.32513 15.204C5.05731 15.4591 4.78949 15.7014 4.54718 15.982C4.154 16.4378 3.72774 16.8641 3.27186 17.2573C3.0895 17.4053 2.91894 17.5674 2.76173 17.7419C2.62033 17.9905 2.43444 18.2109 2.21334 18.3923C2.04107 18.4996 1.8424 18.557 1.63945 18.5581C1.42717 18.5539 1.22001 18.4922 1.04004 18.3796C0.491656 18.0225 -0.0694852 17.4231 0.00703411 16.8237C0.0835534 16.2243 0.211085 16.237 1.14207 15.306L1.30786 15.1657L1.76698 14.7576C2.0603 14.5153 2.35363 14.273 2.62144 14.0052C3.19534 13.4313 3.92227 12.7681 4.39414 12.36C4.86601 11.9519 5.33788 11.5311 5.79699 11.0847Z" fill="#4EA3AA"/>
                  </svg>
                </a>
              </p>
            </div>
          </div>
        </div>
      </aside><!-- End of sidebar -->

    </div>
  </div><!-- End of page-content -->

  <div class="container">
    <div class="call-to-action mb-0.5 md:mb-20">
      <div class="text bg-aqua-pattern">
        <p class="text-white font-400 mb-8">Want to organize your own contest? The Rube Goldberg Challenge offers hosts the chance to host an RGIIC-authorized contest in your community.</p>
        <div class="btn-row"><a href="#" class="btn block-white">Host Your Own</a></div>
      </div>
      <div class="image"><img src="./img/placeholder/d-contents-rgmc-detail-cta.png" /></div>
    </div><!-- End of call-to-action -->
  </div>
</main><!-- End of main -->

<?php include "./footer.html"; ?>