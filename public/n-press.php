<?php include "./header.html"; ?>

<main id="main">
    <div class="page-banner bleed">
      <div class="container">
        <div class="image-wrap"><img src="./img/placeholder/press-banner.jpg" alt="Jennifer George featured in the New York Times" /></div>
        <div class="image-caption caption">Jennifer George featured in the New York Times</div>
      </div>
    </div><!-- End of page-banner -->

  <div class="page-content py-10 md:pt-16 md:pb-20">
    <div class="container">
      <article class="content">
        <div class="text-block float on-full bg-off-white">
          <h1 class="page-title">Press</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus facilisis euismod eget faucibus facilisi. Elementum, nisl ipsum massa urna ornare semper dolor aenean. Tempor eu neque enim, sem sed etiam volutpat morbi pharetra. Donec lacus senectus suspendisse viverra. Non enim enim facilisis nulla quis.</p>
        </div>

        <div class="wrapper grid grid-cols-1 gap-y-9 gap-x-0 md:grid-cols-2 md:gap-9 lg:grid-cols-3 lg:gap-9">
            <div class="card v6">
                <a href="#" class="image" title="Digital Journal" ><img src="./img/placeholder/digital-journal.svg" alt="Digital Journal" /></a>
                <div class="details">
                    <h2 class="h5 mb-0">Digital Journal</h2>
                    <p class="mb-0">A Rube Goldberg Hand-Washing Contraption? The Race Is On</p>
                </div>
            </div>
            <div class="card v6">
                <a href="#" class="image" title="The New York Times" ><img src="./img/placeholder/the-newyork-times.svg" alt="The New York Times" /></a>
                <div class="details">
                    <h2 class="h5 mb-0">The New York Times</h2>
                    <p class="mb-0">A Rube Goldberg Hand-Washing Contraption? The Race Is On</p>
                </div>
            </div>
            <div class="card v6">
                <a href="#" class="image" title="MuseumLab" ><img src="./img/placeholder/museum-lab.svg" alt="MuseumLab" /></a>
                <div class="details">
                    <h2 class="h5 mb-0">MuseumLab</h2>
                    <p class="mb-0">Amuseum – An Augmented Reality Rube Goldberg Experience</p>
                </div>
            </div>
            <div class="card v6">
                <a href="#" class="image" title="Digital Trends" ><img src="./img/placeholder/digital-trends.svg" alt="Digital Trends" /></a>
                <div class="details">
                    <h2 class="h5 mb-0">Digital Trends</h2>
                    <p class="mb-0">How Rube Goldberg’s granddaughter keeps the legacy of crazy contraptions alive</p>
                </div>
            </div>
            <div class="card v6">
                <a href="#" class="image" title="The Telegraph" ><img src="./img/placeholder/Telegraph.svg" alt="The Telegraph" /></a>
                <div class="details">
                    <h2 class="h5 mb-0">The Telegraph</h2>
                    <p class="mb-0">The man behind the contraptions: how Rube Goldberg found the cure for self-isolation boredomr</p>
                </div>
            </div>
            <div class="card v6">
                <a href="#" class="image" title="Lifehacker" ><img src="./img/placeholder/lifehacker.svg" alt="Lifehacker" /></a>
                <div class="details">
                    <h2 class="h5 mb-0">Lifehacker</h2>
                    <p class="mb-0">Now Is the Time to Build a Ridiculous Contraption</p>
                </div>
            </div>
        </div>
      </article><!-- End of content -->
    </div>
  </div><!-- End of page-content -->

</main><!-- End of main -->

<?php include "./footer.html"; ?>