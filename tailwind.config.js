module.exports = {
  content: [
    './public/**/*.{html,php}',
  ],
  theme: {
    container:{
      center: true,
      padding: '1.75rem',
      screens: {
        sm: "100%",
        md: "100%",
        lg: "100%",
        xl: "1256px"
      }
    },
    extend: {
      fontFamily: {
        'display': ['"Nimbus Sans"', 'sans-serif'],
        'body': ['"Nimbus Sans"', 'sans-serif'],
      },
      colors:{
        'aqua': '#63AEB5',
        'soft-black': '#1E1E1D',
        'darker-gray': '#848580',
        'dark-gray': '#90928C',
        'mid-gray': '#BCBCB8',
        'light-gray': '#E7E7DF',
        'off-white': '#F2F2EE',
      },
      backgroundImage: {
        'aqua-pattern': "url('./img/background/aqua-bg-pattern.svg')",
        'illustration-knife': "url('./img/background/Illustration-Knife-TNT.svg')",
        'illustration-vice': "url('./img/background/Illustration-vice-sketch.svg')",
        'illustration-pale': "url('./img/background/Illustration-pale.svg')",
        'soft-black-pattern': "url('./img/background/soft-black-bg-pattern.svg')",
        'bird-hat': "url('./img/background/hero-image.svg')",
      },
      backgroundPosition: {
        right: 'right',
        'right-bottom-10-30': 'right 10% bottom 30%',
      }
    },
  },
  plugins: [],
}
