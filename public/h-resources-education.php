<?php include "./header.html"; ?>

<main id="main">
    <div class="page-banner bleed">
      <div class="container">
        <div class="image-wrap"><img src="./img/placeholder/Rube-Resources.jpg" alt="Rube Resources" /></div>
      </div>
    </div><!-- End of page-banner -->

  <div class="page-content py-10 md:pt-16 md:pb-20 ">
    <div class="container bg-no-repeat lg:bg-[right_15rem_bottom_0rem] lg:bg-[length:160px_auto] lg:bg-illustration-pale">
      <article class="content">
        <div class="text-block auto -mt-24 lg:-mt-48 on-full bg-off-white">
          <h1 class="page-title">Rube Resources</h1>
          <p>Rube Goldberg’s legacy reminds us that through informed observation, creative thinking, artistic response, problem-solving, curiosity, and inventiveness, we can use available resources to change the world.  The Institute champions these values, inviting people of all ages to discover their full potential through the power of these ideals. </p>
        </div>

        <div class="wrapper mb-10 md:mb-12 flex flex-wrap justify-between items-start gap-y-0 gap-x-4">
            <div class="tabs basis-full lg:basis-4/5">
                <a href="#" class="toggle block lg:hidden"><span class="text">All</span></a> 
                <ul class="uppercase xl">
                    <li><a href="#">Projects</a></li>
                    <li><a href="#">Books</a></li>
                    <li><a href="#">Games & Coding</a></li>
                    <li><a href="#" class="active">Education</a></li>
                </ul>
            </div>
        </div>


        <div class="card v4 mb-10 md:mb-16">
            <div class="divider hidden md:block mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h4 mb-2.5">Speed-Build Rube</h2>
                  <p class="mb-7">Our official Rube Goldberg Speed-Build Bag® is filled with all the things you’ll need to build your first RGM’s. This starter-kit comes with two machine task options. One to TRAP A MOUSE and the other to POP A BALLOON. Just add the contents of the bag to a table of random household junk and let the fun begin! Designed for up to 15 builders, ages 4 and over.</p>
                  <div class="btn-wrap">
                      <a href="#" class="btn outline-black">Order the Bag</a>
                  </div>
                </div>
                <div class="image"><img src="./img/placeholder/education-speed-build.png" alt="Speed-Build Rube" /></div>
            </div>
        </div>
        <div class="card v4 mb-10 md:mb-16">
            <div class="divider hidden md:block mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h4 mb-2.5">Rube Goldberg iBlock Project</h2>
                  <p class="mb-7">An iBlock is an exciting way to teach students important STEM concepts while practicing problem-solving and perseverance. The iBlock is available to teachers, schools, and districts to help kids get from point “A” to “B'' as they imagine, design and build their inventions. The iBlock includes everything you’ll need to get going, from the learning content, consumables, and materials you’ll use to build your machines. Try a free sample of the Rube Goldberg iBlock to see how it can work for your classroom.</p>
                  <div class="btn-wrap">
                      <a href="#" class="btn outline-black">Get the iBlock</a>
                  </div>
                </div>
                <div class="image"><img src="./img/placeholder/education-iblock-project.png" alt="Rube Goldberg iBlock Project" /></div>
            </div>
        </div>
        <div class="card v4">
            <div class="divider mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h4 mb-2.5">Education Activities & Lesson Plans [FPO]</h2>
                  <p class="mb-7">A Rube Goldberg contraption is essentially a green machine. They cost nothing to build, are made from found objects, and invite problem-solvers to harvest the best available materials and tools to create a working Rube Goldberg Machine. The Institute partners with Teq Education Services to provide equitable and innovative activities and lesson plans designed to help you get the most out of building Rube Goldberg into your curriculum.</p>
                  <div class="btn-wrap mb-7">
                      <a href="#" class="btn outline-black">Download Lessons</a>
                  </div>
                  <p><strong>Lesson PDF Contents</strong></p>
                  <ul class="type-none pl-0">
                    <li>Lesson 1: Draw a Simple Machine Cartoon</li>
                    <li>Lesson 2: Human Rube Goldberg Machine</li>
                    <li>Lesson 3: Experiment with Simple Machines</li>
                    <li>Lesson 4: Machine Poetry</li>
                    <li>Lesson 5: Machine Stories</li>
                    <li>Lesson 6: Energy Transfers</li>
                    <li>Lesson 7: Build a 3-Step Rube Goldberg Machine</li>
                    <li>Lesson 8: Build a 7-Step Rube Goldberg Machine</li>
                    <li>Lesson 9: Build a 15-Step Rube Goldberg Machine</li>
                    <li>Lesson 10: Coming Soon</li>
                  </ul>
                </div>
                <div class="image hidden"></div>
            </div>
        </div>

      </article><!-- End of content -->
    </div>
  </div><!-- End of page-content -->

  <div class="container">
    <div class="call-to-action mb-0.5 md:mb-20">
      <div class="text bg-soft-black-pattern">
        <p class="text-white font-400 mb-8">Our <em>Rube Goldberg’s Simple Normal Humdrum School Day</em> book features inventions created to help Rube accomplish mundane daily tasks. Get free lesson plans tied to the book!</p>
        <div class="btn-row"><a href="#" class="btn block-white">Order the Book</a></div>
      </div>
      <div class="image"><img src="./img/placeholder/resources-games-cta.jpg" alt="Order the Book" /></div>
    </div><!-- End of call-to-action -->
  </div>
</main><!-- End of main -->

<?php include "./footer.html"; ?>