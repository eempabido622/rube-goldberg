<?php include "./header.html"; ?>

<main id="main">
  <div class="page-nav hidden lg:block">
    <div class="container" >
      <a href="#" class="nav-link w-min">
        <svg class="icon mr-2.5" width="12" height="18" viewBox="0 0 12 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M5.31922 10.5266L5.17893 10.3991C4.64557 10.0564 4.17651 9.16943 4.17651 8.79443C4.14644 8.74713 4.1182 8.7151 4.09114 8.7029C3.92864 8.62967 4.15885 8.40008 4.29427 8.29443L4.79427 7.79443L5.52327 7.30005L5.98238 6.89194L6.78584 6.17776C6.98989 5.99922 7.16843 5.79517 7.35973 5.60387C7.60296 5.33694 7.86279 5.08563 8.13768 4.85143C8.59288 4.45752 9.01909 4.03131 9.413 3.57611C9.56579 3.3978 9.73207 3.23152 9.91038 3.07873C10.1515 2.94257 10.3673 2.76566 10.548 2.55585C10.8669 2.12224 11.2622 1.89268 10.9816 1.28053C10.6609 0.698636 10.1465 0.247424 9.52778 0.00520491C8.92838 -0.0585612 8.48201 0.477074 8.13768 0.872424L7.97189 1.06372L7.48726 1.59936C7.24495 1.86718 7.01539 2.13499 6.76033 2.3773C6.17368 2.93845 5.42124 3.39756 5.0514 3.86943C4.68155 4.3413 4.05665 5.11925 3.77608 5.38706C3.49551 5.65488 3.0619 6.01197 2.89611 6.1395C2.73031 6.26704 2.60278 6.54761 2.46249 6.67514C2.1054 6.98122 1.74832 7.10875 1.40398 7.44033L1.17442 7.66989C1.03413 7.79742 0.600526 8.06524 0.524007 8.24378C0.447488 8.42233 0.00112438 8.5116 0.00112438 8.7029C-0.00643158 8.90974 0.0194063 9.11644 0.0776434 9.31505C0.128656 9.46809 0.638784 9.74867 0.728057 9.86344C1.00863 10.2078 1.22543 10.2971 1.39122 10.5139C1.68538 10.8036 2.0104 11.0602 2.36047 11.2791C2.66655 11.5214 2.98538 11.7637 3.17667 11.9422C3.62977 12.3611 4.05574 12.8083 4.452 13.2813L5.20444 14.0975L5.79108 14.6459C6.0589 14.901 6.32672 15.1433 6.56903 15.4239C6.96221 15.8797 7.38847 16.306 7.84435 16.6992C8.02671 16.8472 8.19727 17.0093 8.35448 17.1838C8.49588 17.4324 8.68177 17.6528 8.90287 17.8342C9.07514 17.9415 9.27381 17.9989 9.47677 18C9.68904 17.9958 9.8962 17.9341 10.0762 17.8215C10.6246 17.4644 11.1857 16.865 11.1092 16.2656C11.0327 15.6662 10.9051 15.6789 9.97414 14.7479L9.80835 14.6076L9.34923 14.1995C9.05591 13.9572 8.76259 13.7149 8.49477 13.4471C7.92087 12.8732 7.19394 12.21 6.72207 11.8019C6.2502 11.3938 5.77833 10.973 5.31922 10.5266Z" fill="#4EA3AA"/>
        </svg>
        <span class="text">Back</span>
      </a>
    </div>
  </div><!-- End of page-banner -->

  <div class="page-banner">
    <div class="container" >
      <div class="image-wrap"><img src="./img/placeholder/stem-challenge-banner-2x.jpg" /></div>
    </div>
  </div><!-- End of page-banner -->

  <div class="page-content py-10 md:pt-16 md:pb-20">
    <div class="container flex flex-wrap gap-y-10 lg:gap-[8%] bg-no-repeat lg:bg-[right_10rem_bottom_30rem] lg:bg-illustration-knife">
      <article class="content basis-full lg:basis-[55%]">
        <h1 class="page-title">Host Your Own Rube Goldberg STEM Challenge</h1>
        <p>Host your own Rube Goldberg STEM Challenge, create your own contest task, and invite up to 10 teams to participate in an official live Rube Goldberg event. This licensed competition gives you all the tools you’ll need to plan, promote, and run your tournament. This option is designed for schools, museums, libraries, camps, corporate team-building initiatives, or just plain fun! </p>
        
        <div class="widget mb-0 mt-7 image-widget block lg:hidden"><img src="./img/placeholder/contest-badge-RGI-stem-challenge-2x.svg" width="280" height="280"></div>

        <div class="divider my-7 md:my-10 border-t-2 border-soft-black"></div>
        
        <h2 class="text-1_688">Ready to Host your own Rube Goldberg STEM Challenge?</h2>
        <p>Over the past three decades of competition, the institute has developed rules, judging metrics, and forms that promote STEM learning. You will receive it all. Your Rube Goldberg STEM challenge will require machines to adhere to size constraints, include simple machines, run reliably with limited contact, and complete the assigned task.  Arts education, in the form of storytelling, machine design, and construction also factor into the scoring. Social and emotional learning through teamwork and improvisation add to the experience as well. This is an inclusive, all-ages and sustainable live event. All you need is a pile of junk and a great imagination to create a winning Rube Goldberg Machine!</p>
        <p>The STEM Challenge includes:</p>
        <ul>
          <li>Host manual</li>
          <li>Promotional materials</li>
          <li>Fundraising templates</li>
          <li>Rule book</li>
          <li>Judging forms</li>
          <li>Awards package</li>
          <li>Zoom with Team Rube</li>
          <li>Email support</li>
        </ul>
        <p>The PDF below includes STEM Challenge information and registration forms. Please read the competition terms and agreements (opens terms and agreements PDF in new tab) before submitting your registration here. </p>
        <div class="btn-row mb-10 mt-7 md:mt-0 md:mb-16">
          <a href="#" class="btn outline-black mb-2.5 md:mb-0 md:mr-2.5">Challenge Book</a>
          <a href="#" class="btn outline-black mx-0 mr-2.5 md:mx-2.5">Terms & Agreements</a>
        </div>
        <div class="divider mb-7 md:mb-10 border-t-2 border-soft-black"></div>
        <h2 class="text-1_688">Past Tasks for Inspiration</h2>
        <div class="task-table grid grid-cols-1 md:grid-cols-2 gap-4">
          <div class="column">
            <table class="table-auto">
              <tr>
                <td>2023</td>
                <td>TBD</td>
              </tr>
              <tr>
                <td>2022</td>
                <td>Open a Book</td>
              </tr>
              <tr>
                <td>2021</td>
                <td>Shake & Pour a Box of Nerds</td>
              </tr>
              <tr>
                <td>2020</td>
                <td>Turn Off a Light</td>
              </tr>
              <tr>
                <td>2019</td>
                <td>Put Money in a Piggy Bank</td>
              </tr>
              <tr>
                <td>2018</td>
                <td>Pour a Bowl of Cereal</td>
              </tr>
              <tr>
                <td>2017</td>
                <td>Apply a BAND-AID® Brand Adhesive Bandage</td>
              </tr>
              <tr>
                <td>2016</td>
                <td>Open an Umbrella</td>
              </tr>
              <tr>
                <td>2015</td>
                <td>Erase A Chalkboard</td>
              </tr>
              <tr>
                <td>2014</td>
                <td>Zip A Zipper</td>
              </tr>
              <tr>
                <td>2013</td>
                <td>Hammer A Nail</td>
              </tr>
              <tr>
                <td>2012</td>
                <td>Inflate A Balloon & Pop It!</td>
              </tr>
              <tr>
                <td>2011</td>
                <td>Watering A Plant</td>
              </tr>
              <tr>
                <td>2010</td>
                <td>Dispense an Appropriate Amount of Hand Sanitizer into a Hand</td>
              </tr>
              <tr>
                <td>2009</td>
                <td>Replace an Incandescent Light Bulb with a More Energy Efficient Light Bulb</td>
              </tr>
              <tr>
                <td>2008</td>
                <td>Assemble a Hamburger</td>
              </tr>
              <tr>
                <td>2007</td>
                <td>Squeeze the Juice from an Orange</td>
              </tr>
              <tr>
                <td>2006</td>
                <td>Shred 5 Sheets of Paper</td>
              </tr>
            </table>
          </div>
          <div class="column">
            <table class="table-auto">
              <tr>
                <td>2005</td>
                <td>Change Batteries & Turn on a 2-battery Flashlight</td>
              </tr>
              <tr>
                <td>2004</td>
                <td>Select, Mark & Cast an Election Ballot</td>
              </tr>
              <tr>
                <td>2003</td>
                <td>Select, Crush & Recycle and Empty Soft Drink Can</td>
              </tr>
              <tr>
                <td>2002</td>
                <td>Select, Raise & Wave a U.S. Flag</td>
              </tr>
              <tr>
                <td>2001</td>
                <td>Select, Clean & Peel an Apple</td>
              </tr>
              <tr>
                <td>2000</td>
                <td>Fill & Seal a Time Capsule with 20th Century Inventions</td>
              </tr>
              <tr>
                <td>1999</td>
                <td>Set a Golf Tee & Tee Up a Golf Ball</td>
              </tr>
              <tr>
                <td>1998</td>
                <td>Shut Off an Alarm Clock</td>
              </tr>
              <tr>
                <td>1997</td>
                <td>Insert & Then Play a CD Disc</td>
              </tr>
              <tr>
                <td>1996</td>
                <td>Put Coins in a Bank</td>
              </tr>
              <tr>
                <td>1995</td>
                <td>Turn on a Radio</td>
              </tr>
              <tr>
                <td>1994</td>
                <td>Make Cup of Coffee</td>
              </tr>
              <tr>
                <td>1993</td>
                <td>Screw a Light Bulb into a Socket</td>
              </tr>
              <tr>
                <td>1992</td>
                <td>Unlock a Combination Padlock</td>
              </tr>
              <tr>
                <td>1991</td>
                <td>Toast a Slice of Bread</td>
              </tr>
              <tr>
                <td>1990</td>
                <td>Put the Lid on a Ball Jar</td>
              </tr>
              <tr>
                <td>1989</td>
                <td>Sharpen a Pencil</td>
              </tr>
              <tr>
                <td>1988</td>
                <td>Adhere a Stamp to a Letter</td>
              </tr>
              <tr>
                <td>1987</td>
                <td>Put Toothpaste on a Toothbrush</td>
              </tr>
            </table>
          </div>
        </div>
      </article><!-- End of content -->

      <aside class="sidebar basis-full lg:basis-[37%]">
        <div class="widget mb-10 image-widget hidden lg:block"><img src="./img/placeholder/contest-badge-RGI-stem-challenge-2x.svg" width="280" height="280"></div>

        <div class="widget schedule-widget">
          <h3 class="widget-title">Contest Schedule</h3>
          <ul>
            <li>
              <span class="date">Jan 15, 2022</span>
              <span class="info">Registration opens</span>
            </li>
            <li>
              <span class="date">Dec 1, 2022</span>
              <span class="info">Registration closes at 11:59 PM EST</span>
            </li>
            <li>
              <span class="date">Nov 15, 2021</span>
              <span class="info">Team Pages close for editing, unchangeable after 11:59 PM EST</span>
            </li>
          </ul>
        </div>
      </aside><!-- End of sidebar -->
    </div>
  </div><!-- End of page-content -->

  <div class="container">
    <div class="call-to-action mb-0.5 md:mb-20">
      <div class="text bg-aqua-pattern">
        <p class="text-white font-400 mb-8">Looking for inspiration? Browse the gallery of Rube Goldberg invention cartoons! </p>
        <div class="btn-row"><a href="#" class="btn block-white">View the Gallery</a></div>
      </div>
      <div class="image"><img src="./img/placeholder/d-contests-stm-challenge-detail-cta.jpg" /></div>
    </div><!-- End of call-to-action -->
  </div>

</main><!-- End of main -->

<?php include "./footer.html"; ?>