<?php include "./header.html"; ?>

<main id="main">
    <div class="page-nav hidden lg:block">
      <div class="container">
        <a href="#" class="nav-link w-min">
          <svg class="icon mr-2.5" width="12" height="18" viewBox="0 0 12 18" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M5.31922 10.5266L5.17893 10.3991C4.64557 10.0564 4.17651 9.16943 4.17651 8.79443C4.14644 8.74713 4.1182 8.7151 4.09114 8.7029C3.92864 8.62967 4.15885 8.40008 4.29427 8.29443L4.79427 7.79443L5.52327 7.30005L5.98238 6.89194L6.78584 6.17776C6.98989 5.99922 7.16843 5.79517 7.35973 5.60387C7.60296 5.33694 7.86279 5.08563 8.13768 4.85143C8.59288 4.45752 9.01909 4.03131 9.413 3.57611C9.56579 3.3978 9.73207 3.23152 9.91038 3.07873C10.1515 2.94257 10.3673 2.76566 10.548 2.55585C10.8669 2.12224 11.2622 1.89268 10.9816 1.28053C10.6609 0.698636 10.1465 0.247424 9.52778 0.00520491C8.92838 -0.0585612 8.48201 0.477074 8.13768 0.872424L7.97189 1.06372L7.48726 1.59936C7.24495 1.86718 7.01539 2.13499 6.76033 2.3773C6.17368 2.93845 5.42124 3.39756 5.0514 3.86943C4.68155 4.3413 4.05665 5.11925 3.77608 5.38706C3.49551 5.65488 3.0619 6.01197 2.89611 6.1395C2.73031 6.26704 2.60278 6.54761 2.46249 6.67514C2.1054 6.98122 1.74832 7.10875 1.40398 7.44033L1.17442 7.66989C1.03413 7.79742 0.600526 8.06524 0.524007 8.24378C0.447488 8.42233 0.00112438 8.5116 0.00112438 8.7029C-0.00643158 8.90974 0.0194063 9.11644 0.0776434 9.31505C0.128656 9.46809 0.638784 9.74867 0.728057 9.86344C1.00863 10.2078 1.22543 10.2971 1.39122 10.5139C1.68538 10.8036 2.0104 11.0602 2.36047 11.2791C2.66655 11.5214 2.98538 11.7637 3.17667 11.9422C3.62977 12.3611 4.05574 12.8083 4.452 13.2813L5.20444 14.0975L5.79108 14.6459C6.0589 14.901 6.32672 15.1433 6.56903 15.4239C6.96221 15.8797 7.38847 16.306 7.84435 16.6992C8.02671 16.8472 8.19727 17.0093 8.35448 17.1838C8.49588 17.4324 8.68177 17.6528 8.90287 17.8342C9.07514 17.9415 9.27381 17.9989 9.47677 18C9.68904 17.9958 9.8962 17.9341 10.0762 17.8215C10.6246 17.4644 11.1857 16.865 11.1092 16.2656C11.0327 15.6662 10.9051 15.6789 9.97414 14.7479L9.80835 14.6076L9.34923 14.1995C9.05591 13.9572 8.76259 13.7149 8.49477 13.4471C7.92087 12.8732 7.19394 12.21 6.72207 11.8019C6.2502 11.3938 5.77833 10.973 5.31922 10.5266Z" fill="#4EA3AA"/>
          </svg>
          <span class="text">Back</span>
        </a>
      </div>
    </div><!-- End of page-nav -->

    <div class="page-banner">
        <div class="container">
            <div class="image-wrap auto"><img src="./img/placeholder/rube-and-jennifer-hero-2x.jpg" alt="text here" /></div>
            <p class="caption px-7 pt-7 lg:pt-5 mb-0">Rube and his granddaughter Jennifer in Asharoken, Long Island (1962).</p>
        </div>
    </div><!-- End of page-banner -->

    <div class="page-content py-10 md:pt-16 md:pb-20">
        <div class="container">
            <article class="content">
                <div class="wrapper max-w-max mb-10 md:max-w-[54.5%] mx-auto md:mb-24">
                    <h1>“No Limp Fish” — Growing Up with Rube</h1>
                    <p>“No limp fish,” insisted my grandfather, shaking my hand firmly in his. “Harder. Look them in the eye, and let them know they’re meeting someone.” I was a little girl when my grandfather taught me how to shake hands properly — and it made a lasting impression. Crushing palms in my wake, I think of him every time I reach out to meet someone. I’ve met small hands, big hands, strong and weak hands, just fi ngertips, and — as Papa Rube so accurately put it—limp fish. Whenever I speak to a class of fourth- or fifth-graders about my grandfather, they expect to hear myriad crazy contraption adventures—that his house was animated, his kitchen ablaze in gizmos and gadgets, closets fi lled with bowling balls and parrots, and that even the simplest task would involve an elaborate series of pulleys and gears and winches. Instead, or in addition, I teach them how to shake hands. When introducing Rube to a classroom full of kids, I start with fun, little-known facts about my grandfather. Rube and his granddaughter Jennifer in Asharoken, Long Island (1962). Not surprisingly, he loved cars, and he was one of the first people in New York City to own one. </p>
                </div>
                <div class="wrapper flex flex-wrap justify-center gap-[8.2%]">
                    <div class="basis-full mb-10 md:basis-[45.9%] md:mb-24">
                        <div class="gallery-item image">
                            <img src="./img/placeholder/inspired-by-rube-wood-machine-marleyturned-2x.jpg" class="mb-5" alt="Alt Text Here" />
                            <p class="caption mb-0">Jennifer George with “Papa Rube”</p>
                        </div>
                    </div>
                    <div class="basis-full mb-10 md:basis-[45.9%] md:mb-24">
                    <div class="gallery-item image">
                            <img src="./img/placeholder/inspired-by-rube-5yr-crafts-tik-tok-2x.jpg" class="mb-5" alt="Alt Text Here" />
                            <p class="caption mb-0">Drawing with Papa Rube: Rube Goldberg elephant (top) and (bottom) his seven-year-old granddaughter Jennifer’s elephant (1967).</p>
                        </div>
                    </div>
                </div>
                <div class="wrapper flex flex-wrap gap-[5.25%]">
                    <div class="basis-[17.5%] hidden lg:block"></div>
                    <div class="basis-full md:basis-[54.5%] mb-10 md:mb-0">
                        <p>According to family folklore, there were only twelve automobiles in Manhattan around 1910 when Rube acquired his Minerva, a monster of a vehicle that was nearly impossible to steer and required a chauff eur, who sat outside in the elements while wrestling the giant, boat-sized vehicle into submission. I have no idea what happened to that car, but Rube would own many over his lifetime—the last of which was a diesel Mercedes that rumbled and chugged along as if powered by a grumpy herd of buff alo. Rube was born on the Fourth of July in 1883 and died on the anniversary of Pearl Harbor, December 7, 1970. He was a lefty turned righty by his Victorian schoolteachers, yet he always drew with his left hand. He gave memorable, heartfelt eulogies, especially for those he never knew. He didn’t believe in retirement and changed careers at age eighty. He called New York City “the front row,” which I leave to your interpretation—I have my own. His favorite dessert was whipped cream, until the advent of Cool Whip, which he loved even more. He played croquet, smoked Cuban cigars, and wore shoes when swimming. He was always on time. He was always the center of attention. And he had enormous ears. My father, George, and his brother, Tom, were raised in Manhattan, in a town house on West Seventy-Fifth Street. There was a staff of nannies, housekeepers, the chauffeur, and Tessie, their beloved cook, who made the best pecan cookies in the world.* </p>
                        <p>This is where my grandparents held notoriously fabulous parties. I remember hearing how they would send all the furniture to storage and line the walls with paper so their artist friends could vandalize the place with their own form of graffi ti. “The Gershwin boys,” as my grandmother called them, would play piano and sing. Groucho Marx, the Three Stooges, Jack Dempsey, Fanny Brice, and an army of Ziegfeld Girls came and went, while Mayor Jimmy Walker was on hand to make sure that alcohol flowed freely, despite Prohibition. My father and uncle used to tell me how, at one of these illustrious events, Rube brought Charlie Chaplin upstairs to say good night to lorem ipsum dolor.</p>
                    </div>
                    <div class="basis-full md:basis-[40%] lg:basis-[17.5%]">
                        <p><img src="./img/placeholder/widget-image-circle.png" alt="AL text Here" width="200" height="200" /></p>
                        <h2 class="h3 mb-1.5">Have cookies with Rube!</h2>
                        <p>These homemade pecan cookies were his favorite and served at virtually every meal, recipe below.</p>
                        3/4 lb butter<br>
                        1 cup flour<br>
                        1 tablespoon water<br>
                        1 tablespoon vanilla<br>
                        1 cup chopped pecan nuts</p>
                        <p>Mix everything and form dough in the shape of dates. Bake 30–40 minutes in a slow oven (350˚F). After removing from the oven, cool on a rack and sprinkle with 2 heaping tablespoons of powdered sugar. </p>
                    </div>
                </div>
            </article><!-- End of content -->
        </div>
    </div><!-- End of page-content -->

</main><!-- End of main -->

<?php include "./footer.html"; ?>