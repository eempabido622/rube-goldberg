<?php include "./header.html"; ?>

<main id="main">
  <div class="page-content py-10 md:pt-0 md:pb-20">
    <div class="container flex flex-wrap gap-y-10 lg:gap-[3.5%]">
      <article class="content md:pt-10 lg:pt-36 basis-full lg:basis-[55%]">
        <h1 class="page-title mb-7">Zunk. Page Not Found.</h1>
        <p>Looks like you've followed a broken link or entered a URL that doesn't exist on this site. Here are some popular links to help you find what you’re looking for:</p>
        <ul class="type-none pl-0">
            <li><a href="#">Rube Goldberg Contests</a></li>
            <li><a href="#">All About Rube</a></li>
            <li><a href="#">Teaching with Rube</a></li>
            <li><a href="#">About Us</a></li>
            <li><a href="#">Store</a></li>
            <li><a href="#">News & Announcements</a></li>
        </ul>
      </article><!-- End of content -->

      <aside class="sidebar basis-full lg:basis-[41.5%]">
        <div class="widget image-widget mb-0">
            <img src="./img/placeholder/404-image.jpg" alt="Alt Text Here" class="image block w-full h-auto mb-5" />
            <p class="caption mb-0">Boob McNutt gets clunked.</p>
        </div>
      </aside><!-- End of sidebar -->
    </div>
  </div><!-- End of page-content -->

</main><!-- End of main -->

<?php include "./footer.html"; ?>