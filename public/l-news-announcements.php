<?php include "./header.html"; ?>

<main id="main">
  <div class="page-banner">
    <div class="container">
      <div class="image-wrap"><img src="./img/placeholder/news-announcements-banner.jpg" /></div>
    </div>
  </div><!-- End of page-banner -->

  <div class="page-content py-10 md:pt-28 md:pb-20">
    <div class="container flex flex-wrap gap-y-10 lg:gap-[8%]">
      <article class="content basis-full lg:basis-[55%]">
        <div class="text-block float -mt-24 bg-off-white lg:bg-transparent lg:mt-0">
          <h1 class="page-title mb-0">News & Announcements</h1>
        </div>
        
        <div class="card v5 mb-0">
            <div class="divider mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper pb-7">
                <div class="details">
                  <span class="date block text-base text-aqua mb-4">Nov 22, 2022</span>
                  <h2 class="h5 mb-2.5">Miracle<sup>&reg;</sup> Recreation Sets The Rube Goldberg<sup>&reg;</sup> Institute Partnership in Motion With Miracle Machines<sup>&trade;</sup></h2>
                  <p class="mb-0">Lorem ipsum dolro sit atmet oris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit excepteur sint occaecat cupidatat ...</p>
                </div>
                <div class="image"><img src="./img/placeholder/news-thumbnail.jpg" alt="Miracle<sup>&reg;</sup> Recreation Sets The Rube Goldberg® Institute Partnership in Motion With Miracle Machines<sup>&trade;</sup>" /></div>
            </div>
        </div>
        <div class="card v5 full mb-0">
            <div class="divider mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper pb-7">
                <div class="details">
                  <span class="date block text-base text-aqua mb-4">Nov 22, 2022</span>
                  <h2 class="h5 mb-2.5">Miracle<sup>&reg;</sup> Recreation Sets The Rube Goldberg<sup>&reg;</sup> Institute Partnership in Motion With Miracle Machines<sup>&trade;</sup></h2>
                  <p class="mb-0">Lorem ipsum dolro sit atmet oris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit excepteur sint occaecat cupidatat ...</p>
                </div>
            </div>
        </div>
        <div class="card v5 mb-0">
            <div class="divider mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper pb-7">
                <div class="details">
                  <span class="date block text-base text-aqua mb-4">Nov 22, 2022</span>
                  <h2 class="h5 mb-2.5">Miracle<sup>&reg;</sup> Recreation Sets The Rube Goldberg<sup>&reg;</sup> Institute Partnership in Motion With Miracle Machines<sup>&trade;</sup></h2>
                  <p class="mb-0">Lorem ipsum dolro sit atmet oris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit excepteur sint occaecat cupidatat ...</p>
                </div>
                <div class="image"><img src="./img/placeholder/news-thumbnail.jpg" alt="Miracle<sup>&reg;</sup> Recreation Sets The Rube Goldberg® Institute Partnership in Motion With Miracle Machines<sup>&trade;</sup>" /></div>
            </div>
        </div>
        <div class="card v5 mb-0">
            <div class="divider mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper pb-7">
                <div class="details">
                  <span class="date block text-base text-aqua mb-4">Nov 22, 2022</span>
                  <h2 class="h5 mb-2.5">Miracle<sup>&reg;</sup> Recreation Sets The Rube Goldberg<sup>&reg;</sup> Institute Partnership in Motion With Miracle Machines<sup>&trade;</sup></h2>
                  <p class="mb-0">Lorem ipsum dolro sit atmet oris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit excepteur sint occaecat cupidatat ...</p>
                </div>
                <div class="image"><img src="./img/placeholder/news-thumbnail.jpg" alt="Miracle<sup>&reg;</sup> Recreation Sets The Rube Goldberg® Institute Partnership in Motion With Miracle Machines<sup>&trade;</sup>" /></div>
            </div>
        </div>
        <div class="card v5 mb-0">
            <div class="divider mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper pb-7">
                <div class="details">
                  <span class="date block text-base text-aqua mb-4">Nov 22, 2022</span>
                  <h2 class="h5 mb-2.5">Miracle<sup>&reg;</sup> Recreation Sets The Rube Goldberg<sup>&reg;</sup> Institute Partnership in Motion With Miracle Machines<sup>&trade;</sup></h2>
                  <p class="mb-0">Lorem ipsum dolro sit atmet oris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit excepteur sint occaecat cupidatat ...</p>
                </div>
                <div class="image"><img src="./img/placeholder/news-thumbnail.jpg" alt="Miracle<sup>&reg;</sup> Recreation Sets The Rube Goldberg® Institute Partnership in Motion With Miracle Machines<sup>&trade;</sup>" /></div>
            </div>
        </div>
        <div class="card v5 full mb-14">
            <div class="divider mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper pb-7">
                <div class="details">
                  <span class="date block text-base text-aqua mb-4">Nov 22, 2022</span>
                  <h2 class="h5 mb-2.5">Miracle<sup>&reg;</sup> Recreation Sets The Rube Goldberg<sup>&reg;</sup> Institute Partnership in Motion With Miracle Machines<sup>&trade;</sup></h2>
                  <p class="mb-0">Lorem ipsum dolro sit atmet oris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit excepteur sint occaecat cupidatat ...</p>
                </div>
            </div>
        </div>
        <div class="btn-wrap text-center">
            <a href="#" class="btn outline-black min-w-[12.125rem]">View All</a>
        </div>
      </article><!-- End of content -->

      <aside class="sidebar basis-full lg:basis-[37%]">
        <div class="widget event-widget">
            <h3 class="widget-title text-lg mb-5">Featured Event</h3>
            <div class="card v5">
                <a href="#" class="image block mb-2"><img src="./img/placeholder/Featured-Evens-image.jpg" alt="Rube Goldberg: the World of Hilarious Invention Exhibit Opens in Philadelphia" /></a>
                <span class="date block text-lg text-aqua mb-2">Jan 15, 2022</span>
                <h4 class="mb-2"><em>Rube Goldberg: the World of Hilarious Invention Exhibit Opens in Philadelphia</em></h4>
                <p class="mb-0">The Please Touch Museum lorem ipsum dolro sit atmet Rube Goldberg: the World of Hilarious Invention excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
        </div>
      </aside><!-- End of sidebar -->
    </div>
  </div><!-- End of page-content -->

  <div class="container">
    <div class="call-to-action mb-0.5 md:mb-20">
      <div class="text bg-soft-black-pattern">
        <p class="text-white font-400 mb-8">Feature block for press page? consectetur adipiscing elit. Nisi ut ut nec amet ultrices lorem mauris turpis bibendum. Aliquet a enim enim morbi ultrices fusce sollicitudin felis id. </p>
        <div class="btn-row"><a href="#" class="btn block-white">Press</a></div>
      </div>
      <div class="image"><img src="./img/placeholder/news-announcements-cta.jpg" alt="Press" /></div>
    </div><!-- End of call-to-action -->
  </div>
</main><!-- End of main -->

<?php include "./footer.html"; ?>