<?php include "./header.html"; ?>

<main id="main">
    <div class="page-banner bleed">
      <div class="container">
        <div class="image-wrap"><img src="./img/placeholder/Rube-Resources.jpg" alt="Rube Resources" /></div>
      </div>
    </div><!-- End of page-banner -->

  <div class="page-content py-10 md:pt-16 md:pb-20">
    <div class="container">
      <article class="content">
        <div class="text-block auto -mt-24 lg:-mt-48 on-full bg-off-white">
          <h1 class="page-title">Rube Resources</h1>
          <p>Rube Goldberg’s legacy reminds us that through informed observation, creative thinking, artistic response, problem-solving, curiosity, and inventiveness, we can use available resources to change the world.  The Institute champions these values, inviting people of all ages to discover their full potential through the power of these ideals. </p>
        </div>

        <div class="wrapper mb-10 md:mb-12 flex flex-wrap justify-between items-start gap-y-0 gap-x-4">
            <div class="tabs basis-full lg:basis-4/5">
                <a href="#" class="toggle block lg:hidden"><span class="text">All</span></a> 
                <ul class="uppercase xl">
                    <li><a href="#">Projects</a></li>
                    <li><a href="#" class="active">Books</a></li>
                    <li><a href="#">Games & Coding</a></li>
                    <li><a href="#">Education</a></li>
                </ul>
            </div>
        </div>

        <p class="mb-10">Use our illustrated picture books to teach machine thinking & STEM.</p>

        <div class="card v4 mb-10 md:mb-16">
            <div class="divider hidden md:block mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h4 mb-2.5">Rube Goldberg’s Simple Normal Humdrum School Day</h2>
                  <p>If Rube’s inventions are any indication, “normal” means something very different in the Goldberg household. Follow Rube as he sets out on a typical school day, overcomplicating each and every step from the time he wakes up in the morning until the time he goes to bed at night.</p>
                  <p>This book features inventions Rube uses to accomplish mundane daily tasks: a simple way to get ready for school, to make breakfast, to do his homework, and much more.</p>
                  <div class="btn-wrap">
                      <a href="#" class="btn outline-black">Order the Book</a>
                  </div>
                </div>
                <div class="image"><img src="./img/placeholder/resources-book-1.jpg" alt="Rube Goldberg’s Simple Normal Humdrum School Day" /></div>
            </div>
        </div>
        <div class="card v4 mb-10 md:mb-16">
            <div class="divider hidden md:block mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h4 mb-2.5">Rube Goldberg’s Simple Normal Definitely Different Day Off</h2>
                  <p>Follow along as a young Rube Goldberg invents zany chain-reaction contraptions to have the best day off from school ever—including a simple way to play fetch in the yard without leaving his bedroom, a self-operating swing, and a super simple series of movie snacking machines. </p>
                  <div class="btn-wrap">
                      <a href="#" class="btn outline-black">Order the Book</a>
                  </div>
                </div>
                <div class="image"><img src="./img/placeholder/resources-book-2.jpg" alt="Rube Goldberg’s Simple Normal Definitely Different Day Off" /></div>
            </div>
        </div>
        <div class="card v4">
            <div class="divider hidden md:block mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h4 mb-2.5">Rube Goldberg and His Amazing Machines</h2>
                  <p>The hilarious first book in an all-new illustrated middle-grade series starring young inventor Rube Goldberg. On the first day of middle school, Principal Kim announces that the school is going to throw a Contraption Convention — the perfect opportunity for young inventor Rube Goldberg to show off his inventions! But after a fight with his friends, his entry gets off to a rocky start — and then strange incidents begin to throw the town into disarray. Wth the help of his friends, he might just get things back on track and come up with something brilliant before it’s time to face the judging table.</p>
                  <div class="btn-wrap">
                      <a href="#" class="btn outline-black">Order the Book</a>
                  </div>
                </div>
                <div class="image mx-0 md:mx-0"><img src="./img/placeholder/resources-book-3.jpg" alt="Rube Goldberg and His Amazing Machines" width="234" height="350" /></div>
            </div>
        </div>

      </article><!-- End of content -->
    </div>
  </div><!-- End of page-content -->

  <div class="container">
    <div class="call-to-action mb-0.5 md:mb-20">
      <div class="text bg-soft-black-pattern">
        <p class="text-white font-400 mb-8">Order a Build a Bag — our starter kit for building a Rube Goldberg Machine. Great for classrooms, parties, museums or any group of friends who like to BUILD!</p>
        <div class="btn-row"><a href="#" class="btn block-white">Order the Bag</a></div>
      </div>
      <div class="image"><img src="./img/placeholder/resources-books-cta.jpg" alt="Order the Bag" /></div>
    </div><!-- End of call-to-action -->
  </div>
</main><!-- End of main -->

<?php include "./footer.html"; ?>