<?php include "./header.html"; ?>

<main id="main">
    <div class="page-banner bleed">
      <div class="container">
        <div class="image-wrap"><img src="./img/placeholder/Rube-Resources.jpg" alt="Rube Resources" /></div>
      </div>
    </div><!-- End of page-banner -->

  <div class="page-content py-10 md:pt-16 md:pb-20">
    <div class="container">
      <article class="content">
        <div class="text-block auto -mt-24 lg:-mt-48 on-full bg-off-white">
          <h1 class="page-title">Rube Resources</h1>
          <p>Rube Goldberg’s legacy reminds us that through informed observation, creative thinking, artistic response, problem-solving, curiosity, and inventiveness, we can use available resources to change the world.  The Institute champions these values, inviting people of all ages to discover their full potential through the power of these ideals. </p>
        </div>

        <div class="wrapper mb-10 md:mb-12 flex flex-wrap justify-between items-start gap-y-0 gap-x-4">
            <div class="tabs basis-full lg:basis-4/5">
                <a href="#" class="toggle block lg:hidden"><span class="text">All</span></a> 
                <ul class="uppercase xl">
                    <li><a href="#" class="active">Projects</a></li>
                    <li><a href="#">Books</a></li>
                    <li><a href="#">Games & Coding</a></li>
                    <li><a href="#">Education</a></li>
                </ul>
            </div>
        </div>

        <p class="mb-10">Optional introductory text</p>

        <div class="card v4 mb-0">
            <div class="divider mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h3 mb-2.5">Activity Cards</h2>
                  <p>Are you eager to build a Rube Goldberg Machine® but aren’t quite sure where to start? Don’t worry — you’re covered! We partnered with Teaching Things, Inc. powered by Teq (creators of the Rube Goldberg iBlock) to create a set of activity cards that will help you prepare for your build. Use these activities to spark your imagination and let your creativity soar!</p>
                  <ul>
                      <li>Learn vocabulary</li>
                      <li>Complete quick challenges</li>
                      <li>Explore simple machines</li>
                  </ul>
                  <div class="btn-wrap">
                      <a href="#" class="btn outline-black">Get the Cards</a>
                  </div>
                </div>
                <div class="image hidden"></div>
            </div>
        </div>
      </article><!-- End of content -->
    </div>
  </div><!-- End of page-content -->

  <div class="container">
    <div class="call-to-action mb-0.5 md:mb-20">
      <div class="text bg-soft-black-pattern">
        <p class="text-white font-400 mb-8">Rube Goldberg Machine Contests inspire kids and give them hands-on experience with STEM and arts education concepts. Learn more about how to enter!</p>
        <div class="btn-row"><a href="#" class="btn block-white">Learn More</a></div>
      </div>
      <div class="image"><img src="./img/placeholder/Rube-Resources-cta.jpg" alt="Learn More" /></div>
    </div><!-- End of call-to-action -->
  </div>
</main><!-- End of main -->

<?php include "./footer.html"; ?>