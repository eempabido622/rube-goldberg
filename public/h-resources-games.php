<?php include "./header.html"; ?>

<main id="main">
    <div class="page-banner bleed">
      <div class="container">
        <div class="image-wrap"><img src="./img/placeholder/Rube-Resources.jpg" alt="Rube Resources" /></div>
      </div>
    </div><!-- End of page-banner -->

  <div class="page-content py-10 md:pt-16 md:pb-20">
    <div class="container">
      <article class="content">
        <div class="text-block float on-full bg-off-white">
          <h1 class="page-title">Rube Resources</h1>
          <p>If you’re a parent, an educator, a student or just Rube-curious, you’re in the right place! The Institute proudly shares Rube Goldberg projects, activities, PD and curriculum in both the physical and virtual space.  </p>
        </div>

        <div class="wrapper mb-10 md:mb-12 flex flex-wrap justify-between items-start gap-y-0 gap-x-4">
            <div class="tabs basis-full lg:basis-4/5">
                <a href="#" class="toggle block lg:hidden"><span class="text">All</span></a> 
                <ul class="uppercase xl">
                    <li><a href="#">Projects</a></li>
                    <li><a href="#">Books</a></li>
                    <li><a href="#" class="active">Games & Coding</a></li>
                    <li><a href="#">Education</a></li>
                </ul>
            </div>
        </div>

        <p class="mb-10">Use coding, digital games and AR to teach Rube.</p>

        <div class="card v4 mb-10 md:mb-16">
            <div class="divider hidden md:block mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h3 mb-2.5">Digital Puzzle Game: Rube Works</h2>
                  <p>Do you have what it takes to turn Rube’s humorous comics and cartoons into elaborate and incredible machines? Rube Works features Rube’s cartoons, contraptions, and irreverent humor and animates the iconic invention cartoons in a way never seen before. The game combines the puzzle genre with slapstick humor and creative problem solving. Try level 1 here!</p>
                  <p  class="mb-10">The app is available on Apple, Google Play, Amazon, and STEAM®</p>

                  <h3 class="h5 mb-2.5">Using Rube Works in the Classroom</h3>
                  <p>Lesson plans are included in the current version of Rube Works via the Help button. Educational discounts (50% off) are available when purchasing 20 or more units. For Steam educational discounts, please contact us.</p>
                  <p  class="mb-10">Want to learn more about how we designed and developed Rube Works? Contact David Fox, the Rube Works game designer and co-executive producer to set up a virtual Q&A for your class.</p>
                  <h4 class="h5 mb-2.5">Rube Works is Endorsed by</h4>

                  <div class="wrapper grid grid-cols-4 gap-4 lg:gap-x-4 lg:gap-y-9 items-center">
                      <div class="item"><a href="#"><img src="./img/placeholder/common-sense-media.svg" alt="Common Sense Media" class="mx-auto" /></a></div>
                      <div class="item"><a href="#"><img src="./img/placeholder/C_Net.svg" alt="CNET" class="mx-auto" /></a></div>
                      <div class="item"><a href="#"><img src="./img/placeholder/NSTA.svg" alt="NSTA" class="mx-auto" /></a></div>
                      <div class="item"><a href="#"><img src="./img/placeholder/teacher-w-apps.svg" alt="Teacher With Apps" class="mx-auto" /></a></div>
                      <div class="item"><a href="#"><img src="./img/placeholder/stem.svg" alt="Stem" class="mx-auto" /></a></div>
                  </div>
                </div>
                <div class="image"><img src="./img/placeholder/resources-games-1.jpg" alt="Digital Puzzle Game: Rube Works" /></div>
            </div>
        </div>
        <div class="card v4 mb-10 md:mb-16">
            <div class="divider mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h3 mb-2.5">Build a Rube Goldberg Machines in AR</h2>
                  <p>Log onto Gadgeteer and enter your virtual reality Rube Goldberg room to build a 3-dimensional RGM.</p>
                  <div class="btn-wrap">
                      <a href="#" class="btn outline-black">Visit Gadgeteer</a>
                  </div>
                </div>
                <div class="image hidden"></div>
            </div>
        </div>
        <div class="card v4">
            <div class="divider mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h3 mb-2.5">Learn to Code</h2>
                  <p>The Apple Coding Guide takes the chain reaction principles of Rube Goldberg to teach coding.</p>
                  <div class="btn-wrap">
                      <a href="#" class="btn outline-black">Visit Apple Coding</a>
                  </div>
                </div>
                <div class="image hidden"></div>
            </div>
        </div>
      </article><!-- End of content -->
    </div>
  </div><!-- End of page-content -->

  <div class="container">
    <div class="call-to-action mb-0.5 md:mb-20">
      <div class="text bg-soft-black-pattern">
        <p class="text-white font-400 mb-8"><em>Rube Goldberg’s Simple Normal Definitely Different Day Off</em> features the improbable inventions of young Rube as he enjoys a fun-filled day off. Get free lesson plans tied to the book!</p>
        <div class="btn-row"><a href="#" class="btn block-white">Order</a></div>
      </div>
      <div class="image"><img src="./img/placeholder/resources-games-cta.jpg" alt="Order" /></div>
    </div><!-- End of call-to-action -->
  </div>
</main><!-- End of main -->

<?php include "./footer.html"; ?>