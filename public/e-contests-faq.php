<?php include "./header.html"; ?>

<main id="main">
    
  <div class="page-nav hidden lg:block">
    <div class="container">
      <a href="#" class="nav-link w-min">
        <svg class="icon mr-2.5" width="12" height="18" viewBox="0 0 12 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M5.31922 10.5266L5.17893 10.3991C4.64557 10.0564 4.17651 9.16943 4.17651 8.79443C4.14644 8.74713 4.1182 8.7151 4.09114 8.7029C3.92864 8.62967 4.15885 8.40008 4.29427 8.29443L4.79427 7.79443L5.52327 7.30005L5.98238 6.89194L6.78584 6.17776C6.98989 5.99922 7.16843 5.79517 7.35973 5.60387C7.60296 5.33694 7.86279 5.08563 8.13768 4.85143C8.59288 4.45752 9.01909 4.03131 9.413 3.57611C9.56579 3.3978 9.73207 3.23152 9.91038 3.07873C10.1515 2.94257 10.3673 2.76566 10.548 2.55585C10.8669 2.12224 11.2622 1.89268 10.9816 1.28053C10.6609 0.698636 10.1465 0.247424 9.52778 0.00520491C8.92838 -0.0585612 8.48201 0.477074 8.13768 0.872424L7.97189 1.06372L7.48726 1.59936C7.24495 1.86718 7.01539 2.13499 6.76033 2.3773C6.17368 2.93845 5.42124 3.39756 5.0514 3.86943C4.68155 4.3413 4.05665 5.11925 3.77608 5.38706C3.49551 5.65488 3.0619 6.01197 2.89611 6.1395C2.73031 6.26704 2.60278 6.54761 2.46249 6.67514C2.1054 6.98122 1.74832 7.10875 1.40398 7.44033L1.17442 7.66989C1.03413 7.79742 0.600526 8.06524 0.524007 8.24378C0.447488 8.42233 0.00112438 8.5116 0.00112438 8.7029C-0.00643158 8.90974 0.0194063 9.11644 0.0776434 9.31505C0.128656 9.46809 0.638784 9.74867 0.728057 9.86344C1.00863 10.2078 1.22543 10.2971 1.39122 10.5139C1.68538 10.8036 2.0104 11.0602 2.36047 11.2791C2.66655 11.5214 2.98538 11.7637 3.17667 11.9422C3.62977 12.3611 4.05574 12.8083 4.452 13.2813L5.20444 14.0975L5.79108 14.6459C6.0589 14.901 6.32672 15.1433 6.56903 15.4239C6.96221 15.8797 7.38847 16.306 7.84435 16.6992C8.02671 16.8472 8.19727 17.0093 8.35448 17.1838C8.49588 17.4324 8.68177 17.6528 8.90287 17.8342C9.07514 17.9415 9.27381 17.9989 9.47677 18C9.68904 17.9958 9.8962 17.9341 10.0762 17.8215C10.6246 17.4644 11.1857 16.865 11.1092 16.2656C11.0327 15.6662 10.9051 15.6789 9.97414 14.7479L9.80835 14.6076L9.34923 14.1995C9.05591 13.9572 8.76259 13.7149 8.49477 13.4471C7.92087 12.8732 7.19394 12.21 6.72207 11.8019C6.2502 11.3938 5.77833 10.973 5.31922 10.5266Z" fill="#4EA3AA"/>
        </svg>
        <span class="text">Back</span>
      </a>
    </div>
  </div><!-- End of page-nav -->

    <div class="page-banner bleed">
        <div class="container">
        <div class="image-wrap"><img src="./img/placeholder/FAQ-banner.jpg" alt="Frequently Asked Questions" /></div>
        </div>
    </div><!-- End of page-banner -->

  <div class="page-content py-10 md:pt-16 md:pb-28">
    <div class="container">
      <article class="content">
        <h1 class="page-title mb-5">Frequently Asked Questions</h1>
        <p class="lg:max-w-[66%] mb-8">We organize a lot of contests! We also get asked a lot of questions about how those contests work. Below is a list of the four competitions we offer and the most frequently asked questions that are sent to us. Not seeing your question here? <a href="#">Contact us</a>.</p>
        <div class="wrapper mb-7 flex flex-wrap justify-between items-start gap-y-0 gap-x-4">
            <div class="tabs w-full basis-full xl:basis-4/5">
                <a href="#" class="toggle block lg:hidden"><span class="text">All</span></a> 
                <ul class="uppercase xl">
                    <li><a href="#" class="active">General</a></li>
                    <li><a href="#">Machine Contest</a></li>
                    <li><a href="#">Minecraft Contest</a></li>
                    <li><a href="#">Cartoon Contest</a></li>
                    <li><a href="#">STEM Challenge</a></li>
                </ul>
            </div>
        </div>

        <p class="mb-7">Optional explainer text.</p>

        <div class="wrapper grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-[3.125rem]">
            <div class="card">
                <h2 class="h5 mb-3.5">Does our machine have to complete the task?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Do we have to use the Nerds we were sent?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">May we use more than one kind of NERDS in our machine?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Can our RGM shake and pour NERDS more than once?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">What is repeatability?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Are gasses/steam allowed to exit the boundaries of the machine?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Can we utilize a human in our machine?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">What is considered a touch/human intervention?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">What is "a step"?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Can a machine include a remote control device as a step in the machine?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Can a different person or persons give the theatrical/verbal machine presentation, or does it have to be one of two designated people that are running the machine?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Can a team have a banner or prop outside of their area which is not used by the machine?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Do my team members have to be in the same school?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">How are branching steps counted in a machine? Can they count?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Can I enter a machine that has been previously built and posted online?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Can programmable logic controllers or microcontrollers be used?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Is there a limit of plugging in a power strip and running more than two cords inside our machine?</h2>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Placerat egestas eu, accumsan in dignissim ac tincidunt. Pellentesque vulputate egestas venenatis ut euismod lacus augue. Auctor amet vestibulum at sit aliquam pulvinar eu.</p>
            </div>
        </div>
      </article><!-- End of content -->
    </div>
  </div><!-- End of page-content -->

</main><!-- End of main -->

<?php include "./footer.html"; ?>