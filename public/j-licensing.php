<?php include "./header.html"; ?>

<main id="main">
    <div class="page-banner bleed">
      <div class="container">
        <div class="image-wrap"><img src="./img/placeholder/partners-sponsors-banner.jpg" alt="" /></div>
      </div>
    </div><!-- End of page-banner -->

  <div class="page-content py-10 md:pt-16 md:pb-20">
    <div class="container flex flex-wrap gap-y-10 lg:gap-[8%]">
      <article class="content basis-full lg:basis-[55%]">
        <div class="text-block auto -mt-60 pb-0 bg-off-white">
            <h1 class="page-title">Licensing</h1>
            <p>Rube Goldberg's legacy of works (comics, drawings, inventions, imagery), as well as his name, likeness, and Rube Goldberg Machine Contests are trademarked and represented by the Rube Goldberg Institute for Innovation and Creativity. Any use of the aforementioned must be licensed for usage. We encourage educational, non-profit, and commercial reprint and use of Rube's work.</p>
            <p>Licensing fees are based on type of use, and may be waived for educational purposes. If you wish to reprint or reproduce any Rube Goldberg work, adapt Rube Goldberg machines in any format, host Rube Goldberg Machine Contests, or use the Rube Goldberg trademark in any way, including in any media, permission must be secured in advance of any such usage. [Good: 750, Better: 550, Ideal 350]</p>
            <h2 class="h5 mb-2.5">Copyright Language</h2>
            <p>Our copyright language must be used near licensed images: “Artwork Copyright © and TM Rube Goldberg Inc. All Rights Reserved. RUBE GOLDBERG ® is a registered trademark of Rube Goldberg Inc. All materials used with permission. rubegoldberg.org</p>
            <h2 class="h5 mb-2.5">Trademarks</h2>
            <p>Rube Goldberg Machine Contest®, Rube Goldberg Machine® and Rube Goldberg Challenge® are all trademarks of RGIIC. Contest registrations include limited use of Rube Goldberg trademarked content logos, details of which are included in the contest materials.</p>
        </div>
      </article><!-- End of content -->

      <aside class="sidebar basis-full lg:basis-[37%]">
        <div class="widget image-widget">
            <img src="./img/placeholder/Illustration-licensing.svg" class="" alt="ALt Text Here" />
        </div>
      </aside><!-- End of sidebar -->
    </div>

    <div class="container">
        <div class="divider my-10 md:my-20 border-t-2 border-soft-black"></div>
        <div class="wrapper grid grid-cols-1 gap-y-10 md:grid-cols-2 lg:grid-cols-3 md:gap-x-8 md:gap-y-11">
            <div class="card">
                <h2 class="h5 mb-3.5">Image Licensing</h2>
                <p class="mb-0">To use comics or other RGIIC images in publications, or for educational use, please contact <a href="#">Kathleen Felix</a>.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Machine Licensing</h2>
                <p class="mb-0">For adapting a Rube Goldberg Machine, contact <a href="#">Deb Burton Calagna</a>.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Competition Licensing</h2>
                <p class="mb-0">To inquire about the licensing aspect of our contests and usage included in registration fees, get in touch with <a href="#">Cheryll Obendorf</a>.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Commercial Products</h2>
                <p class="mb-0">Have an amazing idea for bringing the spirit of Rube Goldberg to your audiences? Get in touch with <a href="#">Deb Burton Calagna</a>.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Print or Digital Publications</h2>
                <p class="mb-0">For initial inquiries regarding publishing Rube Goldberg's work or RGIIC intellectual property, email <a href="#">Kathleen Felix</a>.</p>
            </div>
            <div class="card">
                <h2 class="h5 mb-3.5">Entertainment</h2>
                <p class="mb-0">Have an amazing idea for bringing the spirit of Rube Goldberg to your audiences? Get in touch with <a href="#">Deb Burton Calagna</a>.</p>
            </div>
        </div>
    </div>
    
  </div><!-- End of page-content -->

<?php include "./footer.html"; ?>