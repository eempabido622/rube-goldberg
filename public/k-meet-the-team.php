<?php include "./header.html"; ?>

<main id="main">
    <div class="page-content py-10 md:pt-0 md:pb-20">
        <div class="container flex flex-wrap gap-y-10 lg:gap-[3.5%]">
            <article class="content md:pt-10 lg:pt-36 basis-full lg:basis-[55%]">
            <h1 class="page-title">Meet the Rube Goldberg <br>Institute Team</h1>
            <p>We proudly work with educators, organizations, and commercial brands to present the world and work of Rube Goldberg through education, entertainment, and cultural endeavors. Passionate about S.T.E.A.M. education, our team is committed to fostering creativity and innovation by offering energizing and skills-building resources and programs. This includes international Rube Goldberg Machine Contests (RGMC) that give students the unique opportunity to identify and imagine, design and build, experiment and improvise. We believe that the work we do at the Rube Goldberg Institute impacts the future of young minds as they embark on solving the very real problems of the world they will inherit. [450]</p>
            </article><!-- End of content -->

            <aside class="sidebar basis-full lg:basis-[41.5%]">
            <div class="widget image-widget mb-0">
                <img src="./img/placeholder/meet-the-team-image.jpg" alt="Alt Text Here" class="image block w-full h-auto mb-5" />
                <p class="caption mb-0">Rube and his granddaughter Jennifer in Asharoken, Long Island (1962).</p>
            </div>
            </aside><!-- End of sidebar -->
        </div>

        <div class="container mt-[3.75rem]">
            <div class="wrapper flex flex-wrap items-start gap-y-10 lg:gap-[4.4%]">
                <div class="basis-full lg:basis-[29.9%] flex flex-wrap grow-0 justify-center lg:justify-end">
                    <img src="./img/placeholder/Jennifer-George.png" alt="Jennifer George" width="220" height="220" />
                </div>
                <div class="basis-full lg:basis-[65.7%]">
                    <h2 class="h4 mb-0">Jennifer George</h2>
                    <h3 class="h4 mb-1.5 font-light">Chief Creative Officer</h3>
                    <p class="mb-0">The world’s foremost expert on all things Rube Goldberg — including his <a href="#">mastery of the handshake</a> – Jennifer George is the chief creative officer of Rube Goldberg Inc. (RGI). Overseeing all aspects of her grandfather’s estate, Jennifer has conceived and developed numerous cultural events and educational projects, often linking the fields of art, science, technology, and entertainment in visionary ways. Annual competitions, traveling museum exhibitions, books, licensing, merchandising, and entertainment opportunities expand and enhance the brand. Jennifer inherited the mantle of running RGI from her father and is focused on keeping the world of Rube Goldberg thriving for generations to come. <br><a href="#">jennifer@rubegoldberg.org</a></p>

                    <div class="wrapper grid grid-cols-1 gap-y-7 md:grid-cols-2 md:gap-x-[1.875rem] md:gap-y-[3.375rem] mt-12">
                        <div class="team-member">
                            <h2 class="h4 mb-0">Deborah Burton Calagna</h2>
                            <h3 class="h4 mb-1.5 font-light">Director of Operations</h3>
                            <p class="mb-0">Questions about how the RGIIC can work with you on special projects in education or entertainment? Looking to sponsor a RGM contests? Deb leads our strategic relationships.<br> <a href="#">deb@rubegoldberg.org</a></p>
                        </div>
                        <div class="team-member">
                            <h2 class="h4 mb-0">Kathleen Felix</h2>
                            <h3 class="h4 mb-1.5 font-light">Licensing Director</h3>
                            <p class="mb-0">If you wonder about images licensing, contest registration, educational discounts, or just "how it works," meet Kathleen. She keeps the wheels rolling!<br> <a href="#">kathleen@rubegoldberg.org</a></p>
                        </div>
                        <div class="team-member">
                            <h2 class="h4 mb-0">Kristen Kelley</h2>
                            <h3 class="h4 mb-1.5 font-light">National Project Director</h3>
                            <p class="mb-0">Kristen supports contest participation for STEAM educators, museums, and students, and acts as a liason for RGM builders.<br> <a href="#">kristen@rubegoldberg.org</a></p>
                        </div>
                        <div class="team-member">
                            <h2 class="h4 mb-0">Cheryll Obendorf</h2>
                            <h3 class="h4 mb-1.5 font-light">National Competition Advisor</h3>
                            <p class="mb-0">Cheryll is our resident expert in running large and successful regional competitions, and an invaluable resource for new teams, new sponsors, and for National Finals in Indiana.<br> <a href="#">cheryll@rubegoldberg.org</a></p>
                        </div>
                    </div>
                    <div class="divider mt-[3.75rem] mb-[2.125rem] border-t-2 border-soft-black"></div>
                    <div class="wrapper grid grid-cols-1 gap-y-7 mt-10 md:grid-cols-2 md:gap-x-[1.875rem] md:gap-y-[3.375rem] md:mt-12">
                        <h2 class="h4 mb-0">General Inquiries</h2>
                        <p>Aren't sure who to contact about your particular Rube Goldberg need or question? Send an email to <a href="#">rube@rubegoldberg.org</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End of page-content -->
</main><!-- End of main -->

<?php include "./footer.html"; ?>