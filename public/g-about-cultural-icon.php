<?php include "./header.html"; ?>

<main id="main">
  <div class="page-banner">
    <div class="container" >
      <div class="image-wrap"><img src="./img/placeholder/cultural-icon-banner.jpg" alt="Alt Text Here" /></div>
    </div>
  </div><!-- End of page-banner -->

  <div class="page-content pb-0 pt-10 md:pt-0 md:pb-20">
    <div class="container flex flex-wrap gap-y-10 lg:gap-[8%] pb-16">
      <article class="content basis-full lg:basis-[55%]">
        <div class="text-block -mt-24 lg:-mt-[14rem] auto bg-off-white">
            <h1 class="page-title">A Cultural Icon</h1>
            <p>Rube Goldberg (1883–1970) was a Pulitzer Prize-winning American cartoonist, inventor, innovator, and the only person whose name is an adjective in Merriam-Webster’s Dictionary. </p>
            <p>Rube’s invention cartoons are only a small part of his life’s work, yet they define his career. A Rube Goldberg Machine® is a satirical chain-reaction contraption, made from everyday objects, that solves a simple problem in the most improbable and inefficient way possible. In their funny functionality, Rube Goldberg’s inventions invite all of us to think more deeply about machines and mechanized processes, gadgets and technologies, and the very human ways in which we use them.
            <p>The digital age has firmly embraced Rube and his influence on pop culture is more evident today than ever. There are hundreds of millions of searches, views and posts across the Internet, but nowhere is his legacy more celebrated than in the <a href="#">official competitions that bear his name.</a> </p>
        </div>
        <p><img src="./img/placeholder/Rube-Quill-&-Ink-Pot.svg" alt="Alt Text Here" /></p>
      </article><!-- End of content -->

      <aside class="sidebar basis-full lg:basis-[37%]">
        <div class="widget image-widget">
            <div class="block relative pt-[14.375rem] mx-auto pb-16 px-10 w-[20rem] h-[28.75rem] bg-contain bg-no-repeat bg-center bg-[url('./img/placeholder/definition-graphic-2x.svg')]">
                <h2 class="text-white h4 mb-1.5">goldberg, rube</h2>
                <p class="text-white mb-1.5">\'gold,berg \ adjective <br>A comically involved, complicated invention, laboriously contrived to perform a simple operation</p>
                <p class="text-white mb-0">— Webster’s New World Dictionary</p>
            </div>
        </div>
      </aside><!-- End of sidebar -->
    </div>

    <div class="container">
        <div class="wrapper flex flex-wrap justify-center md:gap-[8.2%]">
            <div class="basis-full md:basis-[45.9%] mb-10 md:mb-16">
                <div class="gallery-item image">
                    <img src="./img/placeholder/image-self-operating-napkin-g.jpg" class="mb-5" alt="Alt Text Here" />
                    <p class="caption mb-0">Rube Goldberg’s “Self-Operating Napkin,” pen and ink, Collier’s, Sept. 26, 1931.</p>
                </div>
            </div>
            <div class="basis-full md:basis-[45.9%] max-w-full md:max-w-[45.9%] mb-10 md:mb-16 grow-0">
                <div class="gallery-item carousel v2 owl-carousel owl-theme">
                    <div class="item">
                        <img src="./img/placeholder/image-NBC-radio-appearance-1930.jpg" class="mb-5" alt="Alt Text Here" />
                        <p class="caption mb-0">1/4 <br>The Goldberg family (Tom, Rube, Irma, and George) appear on an NBC radio program (c. 1930)</p>
                    </div>
                    <div class="item">
                        <img src="./img/placeholder/image-NBC-radio-appearance-1930.jpg" class="mb-5" alt="Alt Text Here" />
                        <p class="caption mb-0">2/4 <br>The Goldberg family (Tom, Rube, Irma, and George) appear on an NBC radio program (c. 1930)</p>
                    </div>
                    <div class="item">
                        <img src="./img/placeholder/image-NBC-radio-appearance-1930.jpg" class="mb-5" alt="Alt Text Here" />
                        <p class="caption mb-0">3/4 <br>The Goldberg family (Tom, Rube, Irma, and George) appear on an NBC radio program (c. 1930)</p>
                    </div>
                    <div class="item">
                        <img src="./img/placeholder/image-NBC-radio-appearance-1930.jpg" class="mb-5" alt="Alt Text Here" />
                        <p class="caption mb-0">4/4 <br>The Goldberg family (Tom, Rube, Irma, and George) appear on an NBC radio program (c. 1930)</p>
                    </div>
                </div>
            </div>
            <div class="basis-auto mb-10 md:mb-16">
                <div class="gallery-item image">
                    <div class="owl-item">
                        <img src="./img/placeholder/Inline-Image-Up.jpg" width="925" height="262" class="mb-5" alt="Alt Text Here" />
                        <p class="caption mb-0">An early use of the “Professor Butts” name in That’s Life (July 27, 1925</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-banner">
        <div class="image-wrap auto"><img src="./img/placeholder/cultural-icon-content-banner.jpg" alt="" /></div>
    </div><!-- End of page-banner -->

    <div class="container pt-10 md:pt-28">
        <div class="wrapper md:max-w-[54.5%] mx-auto mb-10 md:mb-16">
            <h2 class="mb-5">A New Generation of Rube Fans</h2>
            <p>Rube’s influence on pop culture in the digital age continues to inspire the work of artists, filmmakers, advertisers, builders, and creators. The Institute’s <a href="#">annual competitions</a> reinforce this legacy by inviting even the youngest problem-solvers to Rube with us! </p>
            <iframe width="560" height="368" src="https://www.youtube.com/embed/zPVH2admAuw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="wrapper flex flex-wrap justify-center gap-[3.5%]">
            <div class="basis-full mb-10 md:basis-[45.9%] lg:basis-[41.7%] md:mb-16">
                <blockquote class="blockquote ml-[3.75rem] lg:ml-0">
                    <p class="quote h3 mb-4">A Rube Goldberg Machine is an intentionally delightful waste of time and energy.”</p>
                    <p class="name mb-4">— Jimmy Kimmel, late-night talk show Jimmy Kimmel Live!</p>
                    <div class="divider m-0 w-[9.375rem] border-t-[0.375rem] border-aqua"></div>
                </blockquote>
            </div>
            <div class="basis-full mb-10 md:basis-[45.9%] lg:basis-[54.8%] md:mb-16">
                <iframe width="560" height="368" src="https://www.youtube.com/embed/zPVH2admAuw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>

  </div><!-- End of page-content -->

  <div class="container">
    <div class="call-to-action mb-0.5 md:mb-20">
      <div class="text bg-soft-black-pattern">
        <p class="text-white font-400 mb-8">Rube Goldberg produced almost 50,000 cartoons in his lifetime, which The Institute continues to digitize and publish.</p>
        <div class="btn-row"><a href="#" class="btn block-white">Browse the Gallery</a></div>
      </div>
      <div class="image"><img src="./img/placeholder/d-contests-stm-challenge-detail-cta.jpg" alt="Browse the Gallery" /></div>
    </div><!-- End of call-to-action -->
  </div>

</main><!-- End of main -->

<?php include "./footer.html"; ?>