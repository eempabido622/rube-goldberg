<?php include "./header.html"; ?>

<main id="main">
    <div class="page-banner bleed">
      <div class="container">
        <div class="image-wrap"><img src="./img/placeholder/About-cartoon-gallery.jpg" /></div>
      </div>
    </div><!-- End of page-banner -->

  <div class="page-content py-10 md:pt-16 md:pb-20">
    <div class="container">
      <article class="content">
        <div class="text-block auto -mt-24 lg:-mt-80 on-full bg-off-white">
          <h1 class="page-title">Cartoon Gallery</h1>
          <p>Rube Goldberg's cartoons continue to inspire artists, inventors, dreamers, observers, and social commentators—those whose work transform the way we see our world—just as Rube’s did through the thousands of cartoons he created in his lifetime. Stay tuned, new Rube Goldberg cartoons are added to the website regularly.</p>
          <p>When the institute licenses these images in high-resolution form, they are proprietary. Each of these images requires you to license it before using it. Looking for something specific? Email us. </p>
          <p>
            <a href="#" class="btn-link"><span class="text">About image licensing</span>
                <svg class="icon ml-3" width="12" height="18" viewBox="0 0 12 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.79699 10.5266L5.93728 10.3991C6.47064 10.0564 6.9397 9.16943 6.9397 8.79443C6.96977 8.74713 6.99801 8.7151 7.02507 8.7029C7.18757 8.62967 6.95736 8.40008 6.82195 8.29443L6.32195 7.79443L5.59294 7.30005L5.13383 6.89194L4.33038 6.17776C4.12632 5.99922 3.94778 5.79517 3.75648 5.60387C3.51325 5.33694 3.25342 5.08563 2.97853 4.85143C2.52333 4.45752 2.09712 4.03131 1.70321 3.57611C1.55042 3.3978 1.38414 3.23152 1.20584 3.07873C0.964678 2.94257 0.748934 2.76566 0.568174 2.55585C0.249344 2.12224 -0.146007 1.89268 0.134564 1.28053C0.455328 0.698636 0.969712 0.247424 1.58843 0.00520491C2.18783 -0.0585612 2.6342 0.477074 2.97853 0.872424L3.14433 1.06372L3.62895 1.59936C3.87126 1.86718 4.10082 2.13499 4.35588 2.3773C4.94253 2.93845 5.69497 3.39756 6.06481 3.86943C6.43466 4.3413 7.05956 5.11925 7.34013 5.38706C7.62071 5.65488 8.05431 6.01197 8.22011 6.1395C8.3859 6.26704 8.51343 6.54761 8.65372 6.67514C9.01081 6.98122 9.3679 7.10875 9.71223 7.44033L9.94179 7.66989C10.0821 7.79742 10.5157 8.06524 10.5922 8.24378C10.6687 8.42233 11.1151 8.5116 11.1151 8.7029C11.1226 8.90974 11.0968 9.11644 11.0386 9.31505C10.9876 9.46809 10.4774 9.74867 10.3882 9.86344C10.1076 10.2078 9.89078 10.2971 9.72499 10.5139C9.43083 10.8036 9.10581 11.0602 8.75574 11.2791C8.44967 11.5214 8.13083 11.7637 7.93954 11.9422C7.48644 12.3611 7.06047 12.8083 6.66421 13.2813L5.91177 14.0975L5.32513 14.6459C5.05731 14.901 4.78949 15.1433 4.54718 15.4239C4.154 15.8797 3.72774 16.306 3.27186 16.6992C3.0895 16.8472 2.91894 17.0093 2.76173 17.1838C2.62033 17.4324 2.43444 17.6528 2.21334 17.8342C2.04107 17.9415 1.8424 17.9989 1.63945 18C1.42717 17.9958 1.22001 17.9341 1.04004 17.8215C0.491656 17.4644 -0.0694852 16.865 0.00703411 16.2656C0.0835534 15.6662 0.211085 15.6789 1.14207 14.7479L1.30786 14.6076L1.76698 14.1995C2.0603 13.9572 2.35363 13.7149 2.62144 13.4471C3.19534 12.8732 3.92227 12.21 4.39414 11.8019C4.86601 11.3938 5.33788 10.973 5.79699 10.5266Z" fill="#4EA3AA"/>
                </svg>
            </a>
          </p>
        </div>
        <div class="wrapper mb-7 flex flex-wrap justify-between items-start gap-y-0 gap-x-4">
            <div class="tabs basis-full lg:basis-4/5">
                <a href="#" class="toggle block lg:hidden"><span class="text">All</span></a> 
                <ul class="normal-case">
                    <li><a href="#">All</a></li>
                    <li><a href="#" class="active">Essential Rube</a></li>
                    <li><a href="#">Machines</a></li>
                    <li><a href="#">Foolish Questions</a></li>
                    <li><a href="#">Editorial</a></li> 
                </ul>
            </div>
            <div class="basis-full lg:basis-1/6">
                <p class="text-2xl mb-0 mt-5 lg:mt-0 text-center lg:text-right">137 Items</p>
            </div>
        </div>
        <p class="description mb-9 lg:max-w-[66%]">Our curated collection of Rube’s most popular works.</p>
        <div class="wrapper grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6 lg:gap-y-14 lg:gap-x-[1.875rem]">
            <div class="card v3">
                <a href="#" class="image art gallery-toggle">
                    <img src="./img/placeholder/cartoon-1.png" alt="Bar of Soap Challenge — Toilet Humor, Chanelle Roces, California" />
                </a>
                <div class="details">
                    <p>Lorem Facilisi Ullamcorper Gravida Libero id Lacinia</p>
                </div>
            </div>
            <div class="card v3">
                <a href="#" class="image art gallery-toggle">
                    <img src="./img/placeholder/cartoon-2.png" alt="Bar of Soap Challenge — Toilet Humor, Chanelle Roces, California" />
                </a>
                <div class="details">
                    <p>Lorem Facilisi Ullamcorper Gravida Libero id Lacinia</p>
                </div>
            </div>
            <div class="card v3">
                <a href="#" class="image art gallery-toggle">
                    <img src="./img/placeholder/cartoon-3.png" alt="Bar of Soap Challenge — Toilet Humor, Chanelle Roces, California" />
                </a>
                <div class="details">
                    <p>Lorem Facilisi Ullamcorper Gravida Libero id Lacinia</p>
                </div>
            </div>
            <div class="card v3">
                <a href="#" class="image art gallery-toggle">
                    <img src="./img/placeholder/cartoon-4.png" alt="Bar of Soap Challenge — Toilet Humor, Chanelle Roces, California" />
                </a>
                <div class="details">
                    <p>Lorem Facilisi Ullamcorper Gravida Libero id Lacinia</p>
                </div>
            </div>
            <div class="card v3">
                <a href="#" class="image art gallery-toggle">
                    <img src="./img/placeholder/cartoon-5.png" alt="Bar of Soap Challenge — Toilet Humor, Chanelle Roces, California" />
                </a>
                <div class="details">
                    <p>Lorem Facilisi Ullamcorper Gravida Libero id Lacinia</p>
                </div>
            </div>
            <div class="card v3">
                <a href="#" class="image art gallery-toggle">
                    <img src="./img/placeholder/cartoon-6.png" alt="Bar of Soap Challenge — Toilet Humor, Chanelle Roces, California" />
                </a>
                <div class="details">
                    <p>Lorem Facilisi Ullamcorper Gravida Libero id Lacinia</p>
                </div>
            </div>
            <div class="card v3">
                <a href="#" class="image art gallery-toggle">
                    <img src="./img/placeholder/cartoon-7.png" alt="Bar of Soap Challenge — Toilet Humor, Chanelle Roces, California" />
                </a>
                <div class="details">
                    <p>Lorem Facilisi Ullamcorper Gravida Libero id Lacinia</p>
                </div>
            </div>
            <div class="card v3">
                <a href="#" class="image art gallery-toggle">
                    <img src="./img/placeholder/cartoon-8.png" alt="Bar of Soap Challenge — Toilet Humor, Chanelle Roces, California" />
                </a>
                <div class="details">
                    <p>Lorem Facilisi Ullamcorper Gravida Libero id Lacinia</p>
                </div>
            </div>
            <div class="card v3">
                <a href="#" class="image art gallery-toggle">
                    <img src="./img/placeholder/cartoon-9.png" alt="Bar of Soap Challenge — Toilet Humor, Chanelle Roces, California" />
                </a>
                <div class="details">
                    <p>Lorem Facilisi Ullamcorper Gravida Libero id Lacinia</p>
                </div>
            </div>
            <div class="card v3">
                <a href="#" class="image art gallery-toggle">
                    <img src="./img/placeholder/cartoon-10.png" alt="Bar of Soap Challenge — Toilet Humor, Chanelle Roces, California" />
                </a>
                <div class="details">
                    <p>Lorem Facilisi Ullamcorper Gravida Libero id Lacinia</p>
                </div>
            </div>
            <div class="card v3">
                <a href="#" class="image art gallery-toggle">
                    <img src="./img/placeholder/cartoon-11.png" alt="Bar of Soap Challenge — Toilet Humor, Chanelle Roces, California" />
                </a>
                <div class="details">
                    <p>Lorem Facilisi Ullamcorper Gravida Libero id Lacinia</p>
                </div>
            </div>
            <div class="card v3">
                <a href="#" class="image art gallery-toggle">
                    <img src="./img/placeholder/cartoon-12.png" alt="Bar of Soap Challenge — Toilet Humor, Chanelle Roces, California" />
                </a>
                <div class="details">
                    <p>Lorem Facilisi Ullamcorper Gravida Libero id Lacinia</p>
                </div>
            </div>
        </div>
        <div class="btn-wrap text-center mt-16">
            <a href="#" class="btn outline-black">View All</a>
        </div>
        </article><!-- End of content -->
    </div>
  </div><!-- End of page-content -->

  <div class="container">
    <div class="call-to-action mb-0.5 md:mb-20">
      <div class="text bg-aqua-pattern">
        <p class="text-white font-400 mb-8">We provide innovative resources and S.T.E.A.M. opportunities that empower young people to learn, grow, build confidence and community—and invent the future.</p>
        <div class="btn-row"><a href="#" class="btn block-white">Get Resources</a></div>
      </div>
      <div class="image"><img src="./img/placeholder/winners-gallery-cta-image.png" /></div>
    </div><!-- End of call-to-action -->
  </div>
</main><!-- End of main -->

<div class="gallery-modal bg-white px-0 pt-[4.625rem] md:px-12 md:pt-36 pb-12 lg:px-36">
    <div class="inner px-10 md:px-0">
        <h2 class="mb-4">Winner Video Title or Winner Name</h2>
        <span class="date block text-xl">March 15, 2022</span>
        <p>Optional description text up to 200 characters.</p>
        <div class="media pt-7 mb-10 md:mb-[3.75rem] -mx-10 md:mx-0">
            <img src="./img/placeholder/Cartoon-big.jpg" />
        </div>
        <div class="divider border-t border-mid-gray"></div>
        <div class="page-nav pt-8 pb-10 flex flex-wrap items-center justify-between">
            <a href="#" class="nav-link">
                <svg class="icon" width="12" height="18" viewBox="0 0 12 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.31922 10.5266L5.17893 10.3991C4.64557 10.0564 4.17651 9.16943 4.17651 8.79443C4.14644 8.74713 4.1182 8.7151 4.09114 8.7029C3.92864 8.62967 4.15885 8.40008 4.29427 8.29443L4.79427 7.79443L5.52327 7.30005L5.98238 6.89194L6.78584 6.17776C6.98989 5.99922 7.16843 5.79517 7.35973 5.60387C7.60296 5.33694 7.86279 5.08563 8.13768 4.85143C8.59288 4.45752 9.01909 4.03131 9.413 3.57611C9.56579 3.3978 9.73207 3.23152 9.91038 3.07873C10.1515 2.94257 10.3673 2.76566 10.548 2.55585C10.8669 2.12224 11.2622 1.89268 10.9816 1.28053C10.6609 0.698636 10.1465 0.247424 9.52778 0.00520491C8.92838 -0.0585612 8.48201 0.477074 8.13768 0.872424L7.97189 1.06372L7.48726 1.59936C7.24495 1.86718 7.01539 2.13499 6.76033 2.3773C6.17368 2.93845 5.42124 3.39756 5.0514 3.86943C4.68155 4.3413 4.05665 5.11925 3.77608 5.38706C3.49551 5.65488 3.0619 6.01197 2.89611 6.1395C2.73031 6.26704 2.60278 6.54761 2.46249 6.67514C2.1054 6.98122 1.74832 7.10875 1.40398 7.44033L1.17442 7.66989C1.03413 7.79742 0.600526 8.06524 0.524007 8.24378C0.447488 8.42233 0.00112438 8.5116 0.00112438 8.7029C-0.00643158 8.90974 0.0194063 9.11644 0.0776434 9.31505C0.128656 9.46809 0.638784 9.74867 0.728057 9.86344C1.00863 10.2078 1.22543 10.2971 1.39122 10.5139C1.68538 10.8036 2.0104 11.0602 2.36047 11.2791C2.66655 11.5214 2.98538 11.7637 3.17667 11.9422C3.62977 12.3611 4.05574 12.8083 4.452 13.2813L5.20444 14.0975L5.79108 14.6459C6.0589 14.901 6.32672 15.1433 6.56903 15.4239C6.96221 15.8797 7.38847 16.306 7.84435 16.6992C8.02671 16.8472 8.19727 17.0093 8.35448 17.1838C8.49588 17.4324 8.68177 17.6528 8.90287 17.8342C9.07514 17.9415 9.27381 17.9989 9.47677 18C9.68904 17.9958 9.8962 17.9341 10.0762 17.8215C10.6246 17.4644 11.1857 16.865 11.1092 16.2656C11.0327 15.6662 10.9051 15.6789 9.97414 14.7479L9.80835 14.6076L9.34923 14.1995C9.05591 13.9572 8.76259 13.7149 8.49477 13.4471C7.92087 12.8732 7.19394 12.21 6.72207 11.8019C6.2502 11.3938 5.77833 10.973 5.31922 10.5266Z" fill="#4EA3AA"/>
                </svg>
                <span class="text ml-2.5">Prev</span>
                <div class="image rounded-full overflow-hidden ml-2.5"><img src="./img/placeholder/Ellipse-nav.png" /></div>
            </a>
            <a href="#" class="nav-link">
                <div class="image rounded-full overflow-hidden mr-2.5"><img src="./img/placeholder/Ellipse-nav.png" /></div>
                <span class="text mr-2.5">Next</span>
                <svg class="icon" width="12" height="18" viewBox="0 0 12 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.79699 10.5266L5.93728 10.3991C6.47064 10.0564 6.9397 9.16943 6.9397 8.79443C6.96977 8.74713 6.99801 8.7151 7.02507 8.7029C7.18757 8.62967 6.95736 8.40008 6.82195 8.29443L6.32195 7.79443L5.59294 7.30005L5.13383 6.89194L4.33038 6.17776C4.12632 5.99922 3.94778 5.79517 3.75648 5.60387C3.51325 5.33694 3.25342 5.08563 2.97853 4.85143C2.52333 4.45752 2.09712 4.03131 1.70321 3.57611C1.55042 3.3978 1.38414 3.23152 1.20584 3.07873C0.964678 2.94257 0.748934 2.76566 0.568174 2.55585C0.249344 2.12224 -0.146007 1.89268 0.134564 1.28053C0.455328 0.698636 0.969712 0.247424 1.58843 0.00520491C2.18783 -0.0585612 2.6342 0.477074 2.97853 0.872424L3.14433 1.06372L3.62895 1.59936C3.87126 1.86718 4.10082 2.13499 4.35588 2.3773C4.94253 2.93845 5.69497 3.39756 6.06481 3.86943C6.43466 4.3413 7.05956 5.11925 7.34013 5.38706C7.62071 5.65488 8.05431 6.01197 8.22011 6.1395C8.3859 6.26704 8.51343 6.54761 8.65372 6.67514C9.01081 6.98122 9.3679 7.10875 9.71223 7.44033L9.94179 7.66989C10.0821 7.79742 10.5157 8.06524 10.5922 8.24378C10.6687 8.42233 11.1151 8.5116 11.1151 8.7029C11.1226 8.90974 11.0968 9.11644 11.0386 9.31505C10.9876 9.46809 10.4774 9.74867 10.3882 9.86344C10.1076 10.2078 9.89078 10.2971 9.72499 10.5139C9.43083 10.8036 9.10581 11.0602 8.75574 11.2791C8.44967 11.5214 8.13083 11.7637 7.93954 11.9422C7.48644 12.3611 7.06047 12.8083 6.66421 13.2813L5.91177 14.0975L5.32513 14.6459C5.05731 14.901 4.78949 15.1433 4.54718 15.4239C4.154 15.8797 3.72774 16.306 3.27186 16.6992C3.0895 16.8472 2.91894 17.0093 2.76173 17.1838C2.62033 17.4324 2.43444 17.6528 2.21334 17.8342C2.04107 17.9415 1.8424 17.9989 1.63945 18C1.42717 17.9958 1.22001 17.9341 1.04004 17.8215C0.491656 17.4644 -0.0694852 16.865 0.00703411 16.2656C0.0835534 15.6662 0.211085 15.6789 1.14207 14.7479L1.30786 14.6076L1.76698 14.1995C2.0603 13.9572 2.35363 13.7149 2.62144 13.4471C3.19534 12.8732 3.92227 12.21 4.39414 11.8019C4.86601 11.3938 5.33788 10.973 5.79699 10.5266Z" fill="#4EA3AA"/>
                </svg>
            </a>
        </div><!-- End of page-nav -->
        <div class="text">
            <p>Rube Goldberg's comics and other materials developed by the Rube Goldberg Institute for Innovation and Creativity must be licensed for usage. Licensing fees are based on type of use, and may be waived for educational purposes. If you wish to use Rube Goldberg's artwork or adapt Rube Goldberg machines in any format, permission must be granted in advance for any such usage. Get details on our Licensing, Copyright & Trademark information, or contact us at <a href="mailto:rube@rubegoldberg.org">rube@rubegoldberg.org</a> for general inquries.</p>
        </div>
    </div>
    <a href="#" class="close-modal flex items-center w-min no-underline relative z-20">
        <span class="text font-normal text-sm uppercase mr-6 leading-none close">Close</span>
        <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M10.6067 12.0208L0 22.6274L1.41421 24.0416L12.0209 13.435L22.6275 24.0416L24.0417 22.6274L13.4351 12.0208L24.0416 1.41421L22.6274 0L12.0209 10.6065L1.41433 0L0.000117898 1.41421L10.6067 12.0208Z" fill="#1E1E1D"/>
        </svg>
    </a>
</div>

<?php include "./footer.html"; ?>