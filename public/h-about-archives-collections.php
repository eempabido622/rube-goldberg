<?php include "./header.html"; ?>

<main id="main">
    <div class="page-banner bleed">
      <div class="container">
        <div class="image-wrap"><img src="./img/placeholder/archives-collections-banner.jpg" alt="Archives & Collections" /></div>
      </div>
    </div><!-- End of page-banner -->

  <div class="page-content py-10 md:pt-16 md:pb-20">
    <div class="container">
      <article class="content">
        <div class="text-block auto -mt-24 lg:-mt-80 on-full bg-off-white">
          <h1 class="page-title">Archives & Collections</h1>
          <p>Journalists, humorists, and academic researchers have interviewed, lauded, and analyzed Rube since the 1920s. The RGIIC collects key resources to aid ongoing research into his work and cultural influence.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Turpis viverra tellus arcu nisl quam adipiscing. Tempor sit sit leo et suscipit. Ac ut suscipit cras sit a. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Turpis viverra tellus arcu nisl quam adipiscing. Tempor sit sit leo et suscipit. Ac ut suscipit cras sit a. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Turpis viverra tellus arcu nisl quam.</p>
        </div>

        <div class="card v4 mb-10 md:mb-16">
            <div class="divider hidden md:block mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h4 mb-2.5">Screwball! Rube Goldberg Bibliography and Comics Gallery</h2>
                  <p>Paul Tumey is a Rube Goldberg scholar and co-editor of The Art of Rube Goldberg. His blog Screwball! hosts an extensive bibliography of Rube's works, from comics, to games, to songs, to books. Paul also lists published works about Rube and collections where the bulk of Rube's original artwork may be found.</p>
                  <div class="btn-wrap">
                      <a href="#" class="btn outline-black">Explore the Blog</a>
                  </div>
                </div>
                <div class="image"><img src="./img/placeholder/archives-collection-card-1.jpg" alt="Screwball! Rube Goldberg Bibliography and Comics Gallery" /></div>
            </div>
        </div>
        <div class="card v4 mb-10 md:mb-16">
            <div class="divider hidden md:block mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h4 mb-2.5">William College Museum of Art Collection</h2>
                  <p>Rube's son, the Broadway and film producer George W. George, gave William College 549 Rube originals. George was an alumni of the college (class of 1941).</p>
                  <div class="btn-wrap">
                      <a href="#" class="btn outline-black">View Collection</a>
                  </div>
                </div>
                <div class="image"><img src="./img/placeholder/archives-collection-card-2.jpg" alt="William College Museum of Art Collection" /></div>
            </div>
        </div>
        <div class="card v4 mb-10 md:mb-16">
            <div class="divider hidden md:block mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h4 mb-2.5">Smithsonian Institutions Collections</h2>
                  <p>In 1970, the Smithsonian held a major retrospective exhibit on Rube Goldberg. It was directed by Daniel Boorstin (his first exhibit as the Institution's director) and curated by Peter Marzio (who published a biography of Rube Goldberg in 1973). The Smithsonian collection includes photographs of the exhibit as well as images of the cartoons and other artifacts.</p>
                  <div class="btn-wrap">
                      <a href="#" class="btn outline-black">View Collection</a>
                  </div>
                </div>
                <div class="image"><img src="./img/placeholder/archives-collection-card-3.jpg" alt="Smithsonian Institutions Collections" /></div>
            </div>
        </div>
        <div class="card v4">
            <div class="divider hidden md:block mb-7 border-t-2 border-soft-black"></div>
            <div class="wrapper">
                <div class="details">
                  <h2 class="h4 mb-2.5">UC Berkeley Bancroft Library, Guide to the Rube Goldberg Papers</h2>
                  <p>In 1964, Rube gave a collection of 10,000 pieces of original art, articles, writings, letters, and memorabilia to his alma mater, the University of California. The collection is housed at the Bancroft Library in Berkeley, CA. </p>
                  <p>The collection includes: correspondence; over 5,000 original drawings for comic strips and for editorial cartoons covering national political campaigns, World War II, and postwar international and domestic affairs; clippings; scrapbooks; manuscripts of articles, stories and songs; books written by him; photographs; records; film; artifacts, including a junior plug hat from the University of California, Class of 1904.</p>
                  <div class="btn-wrap">
                      <a href="#" class="btn outline-black">View Papers</a>
                  </div>
                </div>
                <div class="image"><img src="./img/placeholder/archives-collection-card-4.jpg" alt="UC Berkeley Bancroft Library, Guide to the Rube Goldberg Papers" /></div>
            </div>
        </div>

      </article><!-- End of content -->
    </div>
  </div><!-- End of page-content -->

  <div class="container">
    <div class="call-to-action mb-0.5 md:mb-20">
      <div class="text bg-soft-black-pattern">
        <p class="text-white font-400 mb-8">Rube Goldberg Machines appear in beloved films, televison shows, music videos — check out Rube Goldberg TV to see some of the greatest!</p>
        <div class="btn-row"><a href="#" class="btn block-white">Rube Goldberg TV</a></div>
      </div>
      <div class="image"><img src="./img/placeholder/archives-collection-cta.jpg" alt="Rube Goldberg TV" /></div>
    </div><!-- End of call-to-action -->
  </div>
</main><!-- End of main -->

<?php include "./footer.html"; ?>