<?php include "./header.html"; ?>

<main id="main">
  <div class="page-content py-10 md:pt-0 md:pb-20">
    <div class="container flex flex-wrap gap-y-10 lg:gap-[3.5%]">
      <article class="content md:pt-10 lg:pt-36 basis-full lg:basis-[55%]">
        <h1 class="page-title h2 mb-7">Miracle<sup>&reg;</sup> Recreation Sets The Rube Goldberg<sup>&reg;</sup> Institute Partnership in Motion With Miracle Machines<sup>&trade;</sup></h1>
        <p>Nashville, Tenn. (Nov. 19, 2021) -- Miracle Recreation, a pioneer in manufacturing thrilling playgrounds for nearly a century, today announced a strategic partnership with The Rube Goldberg Institute For Innovation & Creativity, the family-run nonprofit that encourages creativity and access to STEM and STEAM education through the creation of comical and overly complicated chain-reaction machines that achieve a simple task. The partnership was brokered by Rube Goldberg’s consumer products agency, Brand Central.</p>
        <p>In the first of many collaborations that celebrate the spirit of noted cartoonist and engineer Rube Goldberg, Miracle will introduce its Miracle Machines™ product line at the American Society of Landscape Architects (ASLA) annual conference and EXPO, at the Music City Center in Nashville, Tennessee. The Miracle Machines are on display at the Miracle booth (#501) tomorrow, Nov. 20 and Sunday, Nov. 21. </p>
        <p>Three oversized, double-sided panels -- “Curiosity Thrilled the Cat”, “My Cup of Tea” and “Goooaaalll” -- allow children to create constant motion using mechanisms such as levers, gears, pegs, balls, and spinners. Designed for ages 2 and up, Miracle Machines encourage collaborative play, while also creating a dynamic and interactive experience that keeps kids engaged for hours. Each panel design embodies Goldberg’s zeitgeist and serve as a gateway into STEM and STEAM learning for children. </p>
        <p>“Miracle is thrilled to kick off this collaboration with The Rube Goldberg Institute that celebrates creativity and inclusiveness, especially in children,” said Mike Sutton, vice president of global sales for Miracle. “We are excited for the Miracle Machines to foster an entirely new level of playground interactivity, becoming in and of themselves destinations for kids. These simple machines help serve as a reminder that STEM and STEAM learning can happen anywhere.” </p>
        <p>As a cultural icon of the 20th century, Rube Goldberg (1883–1970), Pulitzer Prize-winning American cartoonist and innovator, continues to inspire audiences with his inventive genius. Of the estimated 50,000 cartoons he drew in his lifetime, Goldberg is best known for those that depict “Rube Goldberg Machines” -- the chain-reaction contraptions, typically made from everyday objects, that solve a simple task in the most overly complicated, humorous, and inefficient way possible. In their funny functionality, Goldberg’s inventions invite all of us to think more deeply about machines and mechanized processes, gadgets and technologies, and the very human ways in which we use them. </p>
        <iframe class="my-12" width="560" height="368" src="https://www.youtube.com/embed/zPVH2admAuw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <p>Jennifer George, Goldberg’s granddaughter and the chief creative officer of The Rube Goldberg Institute, was instrumental in building the relationship with Miracle and shaping the vision behind the Miracle Machines. </p>
        <p>“This partnership with Miracle is an exciting next extension for the Rube Goldberg brand,” said George. “These first three Miracle Machines bring the whimsy and kinetic energy of my grandfather’s work to life in a fun and tangible way. As this partnership expands, we hope to bring STEM and STEAM education into playgrounds where kids and machines will interact together, making outdoor learning as compelling and educational as being in the classroom.“</p>
        <p>Miracle Machines are a perfect complement to the brand’s currently available Miracle Museum™ collection of products that create a children’s-museum-like experience on the playground. Generally not requiring safety surfacing, Miracle Machines and Miracle Museum commercial products are a perfect way to turn underutilized spaces such as lobbies, waiting areas and shopping centers, into sensory-rich play areas. The Miracle Museum product line is also on display at the ASLA Conference and EXPO.</p>
        <p>“The Miracle Machines project was fun for our industrial designers, and also very challenging, given what’s required of commercial playgrounds from a ruggedness, durability and weather-proofing standpoint,” said Sutton. “Not unlike Rube Goldberg machines, our design team overcame many complicated obstacles to achieve an elegantly simple result: putting smiles on the faces of kids.”</p>
        <p>The Miracle Machines will be available for purchase in the first quarter of 2022. </p>
        <p class="my-10"><img src="./img/placeholder/News-announcements-image.jpg" /></p>
        <h2 class="h4 mb-5">About Miracle® Recreation</h2>
        <p>Founded nearly a century ago, Miracle Recreation inspires communities to develop kids with the character to lead tomorrow through its high-quality playgrounds. A division of PlayPower®, Inc., Miracle Recreation pioneers the world of thrilling, commercial outdoor play equipment with the belief that a lifetime of self-discovery, independence, and leadership begins on a playground. That’s where children take perceived risks, which challenge and transform them, opening their minds to new perspectives. To learn more, visit Miracle-Recreation.com.</p>
        <h2 class="h4 mb-5">About The Rube Goldberg® Institute For Innovation & Creativity </h4>
        <p>The Rube Goldberg Institute stands as a “museum without walls,” offering experiences designed for the 21st Century that span the virtual and physical and introduce people to the rewards of engaging in the arts and sciences. Rube Goldberg’s legacy reminds us of the crucial importance of informed observation, creative thinking, artistic response, problem-solving, curiosity, and inventiveness. The family-run Institute, founded in 1988, proudly works with educators, organizations, and commercial brands to present the world and work of Rube Goldberg through education, entertainment, and cultural endeavors inviting young people from around the world to discover the power of creativity and innovation. For more information, go to RubeGoldberg.org</p>
      </article><!-- End of content -->

      <aside class="sidebar basis-full lg:basis-[41.5%]">
        <div class="widget image-widget mb-14">
            <img src="./img/placeholder/News-announcements-sidebar.jpg" alt="Alt Text Here" class="image block w-full h-auto mb-5" />
            <p class="caption mb-0">Miracle Machines shown left to right: “My Cup of Tea”, “Curiosity Thrilled the Cat”, and “Goooaaalll”.</p>
        </div>
        <div class="widget image-widget mb-14">
            <img src="./img/placeholder/Miracle.svg" alt="Alt Text Here" class="image" width="300" height="auto" />
        </div>
        <div class="widget border-block-widget mb-0">
          <h3 class="widget-title">Optional call to action <br/>lorem ipsum</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ac nunc venenatis scelerisque et eget velit non. Quis habitant neque elementum, ultricies habitasse eget accumsan gravida suscipit. Donec est feugiat.</p>
          <div class="btn-wrap">
            <a href="#" class="btn block-black">Button</a>
            </div>
        </div>
      </aside><!-- End of sidebar -->
    </div>
  </div><!-- End of page-content -->

  <div class="container">
    <div class="call-to-action mb-0.5 md:mb-20">
      <div class="text bg-soft-black-pattern">
        <p class="text-white font-400 mb-8">Feature block for press page? consectetur adipiscing elit. Nisi ut ut nec amet ultrices lorem mauris turpis bibendum. Aliquet a enim enim morbi ultrices fusce sollicitudin felis id. </p>
        <div class="btn-row"><a href="#" class="btn block-white">Press</a></div>
      </div>
      <div class="image"><img src="./img/placeholder/news-announcements-cta.jpg" alt="Press" /></div>
    </div><!-- End of call-to-action -->
  </div>
</main><!-- End of main -->

<?php include "./footer.html"; ?>